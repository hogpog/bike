
/*
 *
 *
 *
 *
 *
 *
 */

#include "Mutex.h"



namespace ONE
{
	Mutex::Mutex()
	{
	#ifdef _WIN32
		m_hMutex = ::CreateMutex(NULL, FALSE, NULL);
	#else
		pthread_mutex_init(&m_hMutex, NULL);
	#endif

		if (!m_hMutex)
		{
			throw Exception("Couldn't create mutex");
		}
	}

	Mutex::~Mutex()
	{
	#ifdef _WIN32
		::CloseHandle(m_hMutex);
	#else
		pthread_mutex_destroy(&m_hMutex);
	#endif
	}

	void Mutex::Lock()
	{
	#ifdef _DEBUG
		Assert(m_hMutex);
	#endif

	#ifdef _WIN32
		if (WaitForSingleObject(m_hMutex, INFINITE) == WAIT_FAILED)
	#else
		if (pthread_mutex_lock(m_hMutex) < 0 )
	#endif
		{
			throw Exception("Couldn't lock the mutex");
		}
	}

	void Mutex::Unlock()
	{
	#ifdef _DEBUG
		Assert(m_hMutex);
	#endif

	#ifdef _WIN32
		if (ReleaseMutex(m_hMutex) != true)
	#else
		if (pthread_mutex_unlock(m_hMutex) < 0)
	#endif
		{
			throw Exception("Couldn't umlock the mutex");
		}
	}
}
