#include "Receiver.hpp"

CReceiver::CReceiver(TCPsocket &_ClientSocket, Player * _player,ONE::Mutex *_mutex)
{
    mutex = _mutex;
    ClientSocket = _ClientSocket;
    player = _player;

}
void CReceiver::newCReceiver(TCPsocket &_ClientSocket, Player * _player,ONE::Mutex *_mutex)
{
    mutex = _mutex;
    ClientSocket = _ClientSocket;
    player = _player;

}
CReceiver::~CReceiver()
{
}

void CReceiver::Run()
{
    while (1)
    {
        char StrRecv[1024];
        if (player->connected)
        {

            ///Zera o variavel senao embuceta num sei pq
            for(unsigned short int x = 0; x< 1024; x++)
            StrRecv[x] = '\0';

            LenCli=SDLNet_TCP_Recv(ClientSocket, StrRecv, 1024);
            mutex->Lock();
            player->bufferReceiver.push_back(StrRecv);
            //printf("%s\n",player->bufferReceiver[0].data());
            if(LenCli<0&&player->connected)
            {
                char logtmp[100];
                sprintf(logtmp,"Cliente ID: %d desconectou\n", player->id);
                logMutex->Lock();
                log->push_back(logtmp);
                logMutex->Unlock();
                player->connected=false;
                SDLNet_TCP_Close(ClientSocket);
                break;
            }
            mutex->Unlock();
        }
        SDL_Delay(1);
    }
}

void CReceiver::getLog(std::deque<std::string> *_log, ONE::Mutex *_logMutex)
{
    log = _log;
    logMutex = _logMutex;
}
