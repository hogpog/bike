
/*
 *
 *
 *
 *
 *
 *
 */

#ifndef _Thread_h
#define _Thread_h

#include "Globals.h"

#ifdef _WIN32
#define STDPREFIX __stdcall
#else
#include <pthread.h>
#define STDPREFIX
#endif



namespace ONE
{
	class ThreadGroup;

	class DLL_EXPORT Thread
	{
		enum Priority
		{
			PRIORITY_LOWEST  = THREAD_PRIORITY_LOWEST,
			PRIORITY_NORMAL  = THREAD_PRIORITY_NORMAL,
			PRIORITY_HIGH    = THREAD_PRIORITY_ABOVE_NORMAL
		};

		public:
			Thread();
			Thread(ThreadGroup* group);
			virtual ~Thread();

			void Start();
			void Wait(unsigned int time);
			void Close();
			void Suspend();
			void Resume();
			void setPriority(int priority);
			int getPriority() const;
			void setThreadGroup(ThreadGroup* group);
			ThreadGroup* getThreadGroup() const;

			virtual void Run() = 0;

		private:
			static unsigned __stdcall Entry(void* pThread);

			int m_Priority;
			ThreadGroup* m_pThreadGroup;
			unsigned int m_idThread;

			#ifdef _WIN32
			HANDLE m_hThread;
			#else

			#endif
	};
}
#endif
