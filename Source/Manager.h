
/*
 *
 *
 *
 *
 *
 */

#ifndef _Manager_h
#define _Manager_h


#include <Ogre.h>
#include <OgreException.h>
#include <OgreSingleton.h>
#include <stdexcept>
#include <boost/shared_ptr.hpp>

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

typedef boost::shared_ptr<Ogre::Root>  spRoot;
typedef boost::shared_ptr<Ogre::SceneManager>  spSceneManager;

class _OgreExport Manager /*: public Ogre::Singleton<Manager>*/
{
public:
    //~Manager(){};
    static spSceneManager getSceneMgr();
    static Ogre::Root* getRoot();
private:
    static void initRoot();
    static void initSceneMgr();

    static Ogre::Root* mRoot;
    static spSceneManager mSceneMgr;

};

#endif
