
/*
 *
 *
 *
 *
 *
 *
 *
 *
 */

#ifndef _Exception_h
#define _Exception_h

#include "Globals.h"
#include <string>



namespace ONE
{
	using std::string;

	class DLL_EXPORT Exception
	{
		public:
			Exception(const char* error, ...);
			const char* what() const;

		private:
			string m_Message;
			int m_iLine;
			int m_iErro;
	};
}

#endif
