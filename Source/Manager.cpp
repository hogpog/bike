#include "Manager.h"

Ogre::Root* Manager::mRoot = new Ogre::Root("Plugins.cfg", "Twisted.cfg", "Graphicslog.txt");
spSceneManager Manager::mSceneMgr = spSceneManager(Manager::getRoot()->createSceneManager(Ogre::ST_GENERIC));

spSceneManager Manager::getSceneMgr()
{
    if (!mSceneMgr)
        initSceneMgr();
    return mSceneMgr;
};

Ogre::Root* Manager::getRoot()
{
    if (!mRoot)
        initRoot();
    return mRoot;
};

void Manager::initRoot()
{
    try
    {
        mRoot = new Ogre::Root("Plugins.cfg", "Twisted.cfg", "Graphicslog.txt");
        if (!mRoot)
        {
            throw std::logic_error("Error: Initializing Root");
        }
    }
    catch (std::logic_error const& le)
    {
        #if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, le.what(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
        #else
            std::cerr << "An exception has occured: " << le.what().c_str() << std::endl;
        #endif
    }
}

void Manager::initSceneMgr()
{
    if (!mRoot)
        initRoot();
    try
    {
        mSceneMgr = spSceneManager(Manager::getRoot()->createSceneManager(Ogre::ST_GENERIC));
        if (!mSceneMgr)
        {
            throw std::logic_error("Error: Initializing SceneManager");
        }
    }
    catch (std::logic_error const& le)
    {
        #if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, le.what(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
        #else
            std::cerr << "An exception has occured: " << le.what().c_str() << std::endl;
        #endif
    }
}
