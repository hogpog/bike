#ifndef _ANIMMODEL_H_
#define _ANIMMODEL_H_

#include <Ogre.h>
#include "Manager.h"

enum AnimTypes
{
    WALK,
    STAND,
    RUN,
    ATTACK,
    JUMP,

    JUMP_ATTACK,
    RUN_ATTACK,
    WALK_ATTACK,
    STAND_ATTACK,

    RUN_JUMP,
    RUN_ATTACK_JUMP
};

class AnimModel
{
public:
    AnimModel(const Ogre::String& sArmor/*,
          const Ogre::String& sGloves,
          const Ogre::String& sArms,
          const Ogre::String& sLegs,
          const Ogre::String& sBoots,
          const Ogre::String& sHips*/);
    bool hasEnded();
    void setEnabled(bool anim);
    void getAnimationState(const Ogre::String& Anim);
    void getAnimationState2(const Ogre::String& Anim);
    void setTimePosition(Ogre::Real timePos);
    void addTime(Ogre::Real timePos);
    void setLoop(bool loop);
    Ogre::SceneNode* getNode();

    Ogre::Entity *Armor;
    Ogre::Entity *Gloves;
    Ogre::Entity *Boots;
    Ogre::Entity *Arms;
    Ogre::Entity *Legs;
    Ogre::Entity *Hips;

    Ogre::AnimationState *AnimArmor;
    Ogre::AnimationState *AnimGloves;
    Ogre::AnimationState *AnimArms;
    Ogre::AnimationState *AnimLegs;
    Ogre::AnimationState *AnimBoots;
    Ogre::AnimationState *AnimHips;

    Ogre::AnimationState *AnimArmor2;
    Ogre::AnimationState *AnimGloves2;
    Ogre::AnimationState *AnimArms2;
    Ogre::AnimationState *AnimLegs2;
    Ogre::AnimationState *AnimBoots2;
    Ogre::AnimationState *AnimHips2;

    AnimTypes Anim;

    Ogre::SceneNode *mMainNode;

    static int numModel;
};

#endif
