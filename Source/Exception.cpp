
/*
 *
 *
 *
 *
 *
 *
 *
 *
 */

#include "Exception.h"

#include <cstdarg>
#include <cstdio>



namespace ONE
{
	Exception::Exception(const char* error, ...)
	{
		char buffer[1024];
		va_list args;

		va_start(args, error);
		vsnprintf(buffer, 1024, error, args);
		va_end(args);

		m_Message = buffer;
	}

	const char* Exception::what() const
	{
		return m_Message.c_str();
	}
}
