#include "AnimModel.h"

int AnimModel::numModel = 0;

AnimModel::AnimModel(const Ogre::String& sArmor/*,
                 const Ogre::String& sGloves,
                 const Ogre::String& sArms,
                 const Ogre::String& sLegs,
                 const Ogre::String& sBoots,
                 const Ogre::String& sHips*/)
{
    Ogre::String numStr = "";
    numStr+=numModel;
    numModel++;
    mMainNode = Manager::getSceneMgr()->getRootSceneNode ()->createChildSceneNode ("mName"+numStr);
    Ogre::Quaternion q(Ogre::Degree(90), Ogre::Vector3::UNIT_Y);

    Armor = Manager::getSceneMgr()->createEntity(sArmor+numStr, sArmor);
    mMainNode->attachObject(Armor);
    mMainNode->rotate(q);
    //mMainNode->setScale(5,5,5);
/*
    Gloves = Manager::getSceneMgr()->createEntity (sGloves+numStr, sGloves);
    mMainNode->attachObject (Gloves);
    mMainNode->rotate(q);

    Arms = Manager::getSceneMgr()->createEntity (sArms+numStr, sArms);
    mMainNode->attachObject (Arms);
    mMainNode->rotate(q);

    Legs = Manager::getSceneMgr()->createEntity (sLegs+numStr, sLegs);
    mMainNode->attachObject (Legs);
    mMainNode->rotate(q);

    Boots = Manager::getSceneMgr()->createEntity (sBoots+numStr, sBoots);
    mMainNode->attachObject (Boots);
    mMainNode->rotate(q);

    Hips = Manager::getSceneMgr()->createEntity (sHips+numStr, sHips);
    mMainNode->attachObject (Hips);
    mMainNode->rotate(q);

    /*
    Head->getSkeleton()->setBlendMode(ANIMBLEND_CUMULATIVE);
    */
    AnimArmor = Armor->getAnimationState("bunneyhop");

    /*AnimGloves = Gloves->getAnimationState("walk_legs");
    AnimArms = Arms->getAnimationState("walk_legs");
    AnimLegs = Legs->getAnimationState("walk_legs");
    AnimBoots = Boots->getAnimationState("walk_legs");
    AnimHips = Hips->getAnimationState("walk_legs");

    AnimArmor2 = Armor->getAnimationState("walk_body_1hand");
    AnimGloves2 = Gloves->getAnimationState("walk_body_1hand");
    AnimArms2 = Arms->getAnimationState("walk_body_1hand");
    AnimLegs2 = Legs->getAnimationState("walk_body_1hand");
    AnimBoots2 = Boots->getAnimationState("walk_body_1hand");
    AnimHips2 = Hips->getAnimationState("walk_body_1hand");*/
};
bool AnimModel::hasEnded()
{
    if(AnimArmor->hasEnded())
    {
        AnimArmor->setEnabled(false);
        return true;
    }
    else
    return false;
}
void AnimModel::setEnabled(bool anim)
{
    if(AnimArmor->hasEnded())
    AnimArmor->setTimePosition(0);
    AnimArmor->setEnabled(anim);
    /*AnimGloves->setEnabled(anim);
    AnimArms->setEnabled(anim);
    AnimLegs->setEnabled(anim);
    AnimBoots->setEnabled(anim);
    AnimHips->setEnabled(anim);

    AnimArmor2->setEnabled(anim);
    AnimGloves2->setEnabled(anim);
    AnimArms2->setEnabled(anim);
    AnimLegs2->setEnabled(anim);
    AnimBoots2->setEnabled(anim);
    AnimHips2->setEnabled(anim);*/
}
void AnimModel::getAnimationState(const Ogre::String& Anim)
{
    AnimArmor  = Armor->getAnimationState(Anim);
    /*AnimGloves = Gloves->getAnimationState(Anim);
    AnimArms  = Arms->getAnimationState(Anim);
    AnimLegs = Legs->getAnimationState(Anim);
    AnimBoots  = Boots->getAnimationState(Anim);
    AnimHips  = Hips->getAnimationState(Anim);*/
}
void AnimModel::getAnimationState2(const Ogre::String& Anim)
{
    AnimArmor2  = Armor->getAnimationState(Anim);
    /*AnimGloves2 = Gloves->getAnimationState(Anim);
    AnimArms2  = Arms->getAnimationState(Anim);
    AnimLegs2 = Legs->getAnimationState(Anim);
    AnimBoots2  = Boots->getAnimationState(Anim);
    AnimHips2  = Hips->getAnimationState(Anim);*/
}
void AnimModel::setTimePosition(Ogre::Real timePos)
{
    AnimArmor->setTimePosition(timePos);
    /*AnimGloves->setTimePosition(timePos);
    AnimArms->setTimePosition(timePos);
    AnimLegs->setTimePosition(timePos);
    AnimBoots->setTimePosition(timePos);
    AnimHips->setTimePosition(timePos);

    AnimArmor2->setTimePosition(timePos);
    AnimGloves2->setTimePosition(timePos);
    AnimArms2->setTimePosition(timePos);
    AnimLegs2->setTimePosition(timePos);
    AnimBoots2->setTimePosition(timePos);
    AnimHips2->setTimePosition(timePos);*/
}
void AnimModel::addTime(Ogre::Real timePos)
{
    AnimArmor->addTime(timePos);
    /*AnimGloves->addTime(timePos);
    AnimArms->addTime(timePos);
    AnimLegs->addTime(timePos);
    AnimBoots->addTime(timePos);
    AnimHips->addTime(timePos);

    AnimArmor2->addTime(timePos);
    AnimGloves2->addTime(timePos);
    AnimArms2->addTime(timePos);
    AnimLegs2->addTime(timePos);
    AnimBoots2->addTime(timePos);
    AnimHips2->addTime(timePos);*/
}
void AnimModel::setLoop(bool loop)
{
    AnimArmor->setLoop(loop);
    /*AnimGloves->setLoop(loop);
    AnimArms->setLoop(loop);
    AnimLegs->setLoop(loop);
    AnimBoots->setLoop(loop);
    AnimHips->setLoop(loop);

    AnimArmor2->setLoop(loop);
    AnimGloves2->setLoop(loop);
    AnimArms2->setLoop(loop);
    AnimLegs2->setLoop(loop);
    AnimBoots2->setLoop(loop);
    AnimHips2->setLoop(loop);*/
}

Ogre::SceneNode* AnimModel::getNode()
{
    return mMainNode;
}
