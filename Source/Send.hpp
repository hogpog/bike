#ifndef _SEND_HPP
#define _SEND_HPP

#include "Thread.h"
#include "SDL_Net.h"
#include "player.hpp"
#include "Mutex.h"

class Send:public ONE::Thread
{
	public:
		Send(TCPsocket &_ClientSocket,Player * _player,ONE::Mutex *_mutex);
		void newSend(TCPsocket &_ClientSocket,Player * _player,ONE::Mutex *_mutex);
		~Send();
		void Run();
	protected:

	private:
        ONE::Mutex *mutex;
        Player *player;
        unsigned int LenCli;
        TCPsocket ClientSocket;

};


#endif
