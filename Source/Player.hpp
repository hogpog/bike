#ifndef _PLAYER_HPP_
#define _PLAYER_HPP_

#include "deque"
#include <string>
#include <sstream>
#include <Ogre.h>
#include "AnimModel.h"
enum Direction
{
    STOPPED,
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};
class Player
{
public:
    int id;
    bool connected;
    bool Forward;

    std::string PlayerName;
    std::deque<std::string> bufferReceiver;
    ///Chat
    std::deque<std::string> stringChat;
    ///Angulo
    unsigned short int AngleY;
    short int AngleX;
    unsigned short int lAngleY;
    ///Posi��es
    Ogre::Vector3 Position;
    Ogre::Vector3 midPosition;
    Player()
            :Position(0,0,0)
    {
        id=0;
        AngleY=0;
        Forward=false;
        model = new AnimModel("backflip.mesh"/*,
                           "luvas.mesh",
                           "bracos.mesh",
                           "pernas.mesh",
                           "botas.mesh",
                           "hips.mesh"*/);
                           //model->getNode()->setScale(5,5,5);
    };
    ~Player(){};
    void update()
    {
        midPosition.x = (midPosition.x+Position.x/10)/2;
        midPosition.y = Position.y/10-2.5f;
        midPosition.z = (midPosition.z+Position.z/10)/2;
        model->getNode()->setPosition(midPosition);
        if(id!=0)
        {
            if(lAngleY!=AngleY)
            {
                model->getNode()->yaw(Ogre::Radian(AngleY));
                lAngleY = AngleY;
            }
        }
    }
    std::deque<std::string> bufferSend;
    AnimModel *model;

private:

};


#endif
