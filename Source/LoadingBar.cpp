
/*
 *
 *
 *
 *
 *
 */
/*
#include "LoadingBar.h"



LoadingBar::LoadingBar()
{
}

LoadingBar::~LoadingBar()
{
}

void LoadingBar::start(RenderWindow* window, unsigned short numGroupsInit,
					   unsigned short numGroupsLoad, Real initProportion)
{
	mWindow = window;
	mNumGroupsInit = numGroupsInit;
	mNumGroupsLoad = numGroupsLoad;
	mInitProportion = initProportion;

	ResourceGroupManager::getSingleton().initialiseResourceGroup("Bootstrap");

	OverlayManager& omgr = OverlayManager::getSingleton();
	mLoadOverlay = (Overlay*)omgr.getByName("Core/LoadOverlay");
	if (!mLoadOverlay)
	{
		OGRE_EXCEPT(Exception::ERR_ITEM_NOT_FOUND,
			"Cannot find loading overlay", "LoadingBar::start");
	}

	mLoadOverlay->show();

	mLoadingBarElement = omgr.getOverlayElement("Core/LoadPanel/Bar/Progress");
	mLoadingCommentElement = omgr.getOverlayElement("Core/LoadPanel/Comment");
	mLoadingDescriptionElement = omgr.getOverlayElement("Core/LoadPanel/Description");

	OverlayElement* barContainer = omgr.getOverlayElement("Core/LoadPanel/Bar");
	mProgressBarMaxSize = barContainer->getWidth();
	mLoadingBarElement->setWidth(0);

	ResourceGroupManager::getSingleton().addResourceGroupListener(this);
}

void LoadingBar::finish()
{
	mLoadOverlay->hide();
	ResourceGroupManager::getSingleton().removeResourceGroupListener(this);
}

void LoadingBar::resourceGroupScriptingStarted(const String& groupName, size_t scriptCount)
{
	assert(mNumGroupsInit > 0 && "You stated you were not going to init "
		"any groups, but you did! Divide by zero would follow...");

		mProgressBarInc = mProgressBarMaxSize * mInitProportion / (Real)scriptCount;
		mProgressBarInc /= mNumGroupsInit;
		mLoadingDescriptionElement->setCaption("Parsing scripts...");
		mWindow->update();
}

void LoadingBar::scriptParseStarted(const String& scriptName)
{
	mLoadingCommentElement->setCaption(scriptName);
	mWindow->update();
}

void LoadingBar::scriptParseEnded(const String& scriptName)
{
	mLoadingBarElement->setWidth(mLoadingBarElement->getWidth() + mProgressBarInc);
	mWindow->update();
}

void LoadingBar::resourceGroupScriptingEnded(const String& groupName)
{
}

void LoadingBar::resourceGroupLoadStarted(const String& groupName, size_t resourceCount)
{
	assert(mNumGroupsLoad > 0 && "You stated you were not going to load "
			"any groups, but you did! Divide by zero would follow...");
	mProgressBarInc = mProgressBarMaxSize * (1-mInitProportion) / (Real)resourceCount;
	mProgressBarInc /= mNumGroupsLoad;
	mLoadingDescriptionElement->setCaption("Loading resources...");
	mWindow->update();
}

void LoadingBar::resourceLoadStarted(const ResourcePtr& resource)
{
	mLoadingCommentElement->setCaption(resource->getName());
	mWindow->update();
}

void LoadingBar::resourceLoadEnded()
{
}

void LoadingBar::worldGeometryStageStarted(const String& description)
{
	mLoadingCommentElement->setCaption(description);
	mWindow->update();
}

void LoadingBar::worldGeometryStageEnded()
{
	mLoadingBarElement->setWidth(mLoadingBarElement->getWidth() + mProgressBarInc);
	mWindow->update();
}

void LoadingBar::resourceGroupLoadEnded(const String& groupName)
{
}
*/
