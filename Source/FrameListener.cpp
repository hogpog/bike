
/*
 *
 *
 *
 *
 *
 */

#include "FrameListener.h"
#include "Ogre.h"

FrameListener::FrameListener(InputManager* InputMgr, Ogre::RenderWindow* wnd,std::vector<Player*> * _player)
        : mInputMgr(InputMgr)
        , mWindow(wnd),pos(0,0,0),
        mRotX(0),mRotY(0)
{
    mDebugOverlay = Ogre::OverlayManager::getSingleton().getByName("Core/DebugOverlay");
    player = _player;
    lastAngleFromX=270;
    angleTemp = 270;
}

FrameListener::~FrameListener()
{

}

bool FrameListener::frameStarted(const Ogre::FrameEvent& event)
{
    if (mInputMgr)
    {
        mInputMgr->Capture();

        OIS::Keyboard* keyboard = static_cast<OIS::Keyboard *>(mInputMgr->getKeyboard());
        OIS::Mouse* mMouse = static_cast<OIS::Mouse *>(mInputMgr->getMouse());
        //const OIS::MouseState ms = mMouse->getMouseState();
        Ogre::Quaternion q(Ogre::Degree(1), Ogre::Vector3::UNIT_Z);
        bool backflip=false;
        bool jump=false;
        std::string tmpMove;
        if (keyboard->isKeyDown(OIS::KC_ESCAPE))
        {
            return false;
        }

        if (keyboard->isKeyDown(OIS::KC_W))
        {
            pos.z+=1;
            dir = FORWARD;
            tmpMove = "F";
        }
        if (keyboard->isKeyDown(OIS::KC_S))
        {
            pos.z-=1;
            dir = BACKWARD;
            tmpMove = "S";
        }
        if (keyboard->isKeyDown(OIS::KC_G))
        {
            backflip = true;
        }
        if (keyboard->isKeyDown(OIS::KC_B))
        {
            (*player)[0]->model->getAnimationState("bunneyhop");
            (*player)[0]->model->setEnabled(true);
            (*player)[0]->model->setLoop(false);
        }
        if (keyboard->isKeyDown(OIS::KC_A))
        {
            mRotY+=event.timeSinceLastFrame*150;
        }
        if (keyboard->isKeyDown(OIS::KC_D))
        {
            mRotY-=event.timeSinceLastFrame*150;
        }
        if (keyboard->isKeyDown(OIS::KC_SPACE))
        {
            jump = true;
        }

        if(!(*player)[0]->model->hasEnded())
            (*player)[0]->model->addTime(event.timeSinceLastFrame);


        const float rotateSpeed = 0.1; ///velocidade de rota��o
        const float fCameraDistance = 12; ///distancia da camera
        mRotY += mMouse->getMouseState().X.rel * -rotateSpeed;
        mRotX += mMouse->getMouseState().Y.rel * -rotateSpeed;

        ///Atualiza posi��o
        (*player)[0]->update();

        const Ogre::Quaternion ElevationQuat(Ogre::Degree(-20), Ogre::Vector3::UNIT_Z);
        const Ogre::Quaternion RotationQuat(Ogre::Degree(mRotY), Ogre::Vector3::UNIT_Y);
        const Ogre::Quaternion CombinedQuat = RotationQuat*ElevationQuat;

        mExCamera->getCamera()->setPosition((*player)[0]->midPosition - CombinedQuat*(fCameraDistance*Ogre::Vector3::UNIT_X));
        mExCamera->getCamera()->setOrientation(CombinedQuat);
        mExCamera->getCamera()->lookAt((*player)[0]->midPosition);
        //if(lYaw != mExCamera->getCamera()->getOrientation().getYaw());
        Ogre::Quaternion quats(-(lYaw-mExCamera->getCamera()->getOrientation().getYaw()),Ogre::Vector3::UNIT_Y);
        Ogre::Quaternion quats2(Ogre::Radian(0.02),Ogre::Vector3::UNIT_Z);
        if(backflip)
        {
            //Ogre::Quaternion quat3 = quats*quats2;
            //(*player)[0]->model->getNode()->rotate(quats2,Ogre::Node::TS_LOCAL);
        }
        angleTarget = (*player)[0]->AngleX;

        if(angleTemp<angleTarget+angleTarget/3)
        {
            angleTemp = angleTarget;
        }
        else if(angleTemp>angleTarget-angleTarget/3)
        {
            angleTemp = angleTarget;
        }

        if(angleTemp<angleTarget)
        {
            angleTemp += angleTarget/3;
        }
        else if(angleTemp>angleTarget)
        {
            angleTemp -= angleTarget/3;
        }
        //printf("Ang: %.2f\n",(*player)[0]->model->getNode()->getOrientation().getYaw().valueAngleUnits());
        //printf("Pitch: %.2f\n",(*player)[0]->model->getNode()->getOrientation().getPitch().valueAngleUnits());
        if((*player)[0]->model->getNode()->getOrientation().getPitch().valueAngleUnits()<=90)
        {
            if(angleTemp>lastAngleFromX)
            {
                if((*player)[0]->model->getNode()->getOrientation().getYaw().valueAngleUnits()>0)
                {
                    (*player)[0]->model->getNode()->pitch(-(Ogre::Radian(lastAngleFromX*3.1415f/180)),Ogre::Node::TS_WORLD);
                    (*player)[0]->model->getNode()->pitch((Ogre::Radian(angleTemp*3.1415f/180)),Ogre::Node::TS_WORLD);
                }
                else
                {
                    (*player)[0]->model->getNode()->pitch((Ogre::Radian(lastAngleFromX*3.1415f/180)),Ogre::Node::TS_WORLD);
                    (*player)[0]->model->getNode()->pitch(-(Ogre::Radian(angleTemp*3.1415f/180)),Ogre::Node::TS_WORLD);
                }
            }
            else
            {
                if((*player)[0]->model->getNode()->getOrientation().getYaw().valueAngleUnits()<0)
                {
                    (*player)[0]->model->getNode()->pitch(-(Ogre::Radian(lastAngleFromX*3.1415f/180)),Ogre::Node::TS_WORLD);
                    (*player)[0]->model->getNode()->pitch((Ogre::Radian(angleTemp*3.1415f/180)),Ogre::Node::TS_WORLD);
                }
                else
                {
                    (*player)[0]->model->getNode()->pitch((Ogre::Radian(lastAngleFromX*3.1415f/180)),Ogre::Node::TS_WORLD);
                    (*player)[0]->model->getNode()->pitch(-(Ogre::Radian(angleTemp*3.1415f/180)),Ogre::Node::TS_WORLD);
                }
            }
        }
        else
        {
            if(angleTemp<lastAngleFromX)
            {
                if((*player)[0]->model->getNode()->getOrientation().getYaw().valueAngleUnits()>0)
                {
                    (*player)[0]->model->getNode()->pitch(-(Ogre::Radian(lastAngleFromX*3.1415f/180)),Ogre::Node::TS_WORLD);
                    (*player)[0]->model->getNode()->pitch((Ogre::Radian(angleTemp*3.1415f/180)),Ogre::Node::TS_WORLD);
                }
                else
                {
                    (*player)[0]->model->getNode()->pitch((Ogre::Radian(lastAngleFromX*3.1415f/180)),Ogre::Node::TS_WORLD);
                    (*player)[0]->model->getNode()->pitch(-(Ogre::Radian(angleTemp*3.1415f/180)),Ogre::Node::TS_WORLD);
                }
            }
            else
            {
                if((*player)[0]->model->getNode()->getOrientation().getYaw().valueAngleUnits()<0)
                {
                    (*player)[0]->model->getNode()->pitch(-(Ogre::Radian(lastAngleFromX*3.1415f/180)),Ogre::Node::TS_WORLD);
                    (*player)[0]->model->getNode()->pitch((Ogre::Radian(angleTemp*3.1415f/180)),Ogre::Node::TS_WORLD);
                }
                else
                {
                    (*player)[0]->model->getNode()->pitch((Ogre::Radian(lastAngleFromX*3.1415f/180)),Ogre::Node::TS_WORLD);
                    (*player)[0]->model->getNode()->pitch(-(Ogre::Radian(angleTemp*3.1415f/180)),Ogre::Node::TS_WORLD);
                }
            }
        }
        lastAngleFromX = angleTemp;
        (*player)[0]->model->getNode()->rotate(quats,Ogre::Node::TS_LOCAL) ;

        //(*player)[0]->model->getNode()->yaw(-(lYaw-mExCamera->getCamera()->getOrientation().getYaw()));
        lYaw = mExCamera->getCamera()->getOrientation().getYaw();
        /*if(backflip)
        {
            (*player)[0]->model->getNode()->rotate((*player)[0]->model->getNode()->getOrientation(),Ogre::Node::TS_PARENT);
        }*/
        //lYaw.valueDegrees();
        std::stringstream stringtmp;
        if(lastAngle != (int)((*player)[0]->model->getNode()->getOrientation().getYaw().valueAngleUnits()))
        {
            stringtmp<<(int)-((*player)[0]->model->getNode()->getOrientation().getYaw().valueAngleUnits());
            tmpMove += "A";
            tmpMove += stringtmp.str();
            lastAngle =  (int)((*player)[0]->model->getNode()->getOrientation().getYaw().valueAngleUnits());
        }
        if(jump)
        {
            tmpMove+="J";
        }
        (*player)[0]->bufferSend.push_back(tmpMove);
        lastString = tmpMove;

        //(*player)[0]->bufferSend[0] += "A"+(int)mExCamera->getCamera()->getOrientation().getYaw().valueDegrees();
        //printf("X = %.2f --- Y = %.2f --- Z = %.2f\n",mCharNode->getPosition().x,mCharNode->getPosition().y,mCharNode->getPosition().z);

        pos.x = 0;
        pos.y = 0;
        pos.z = 0;

    }
    SDL_Delay(1);
    mDebugOverlay->show();

    return true;
}



bool FrameListener::frameEnded(const Ogre::FrameEvent& event)
{
    static Ogre::String currFps = "FPS Atual: ";
    static Ogre::String avgFps = "FPS Medio: ";
    static Ogre::String bestFps = "Melhor FPS: ";
    static Ogre::String worstFps = "Pior FPS: ";
    static Ogre::String tris = "Triangle Count: ";

    try
    {
        Ogre::OverlayElement* guiAvg = Ogre::OverlayManager::getSingleton().getOverlayElement("Core/AverageFps");
        Ogre::OverlayElement* guiCurr = Ogre::OverlayManager::getSingleton().getOverlayElement("Core/CurrFps");
        Ogre::OverlayElement* guiBest = Ogre::OverlayManager::getSingleton().getOverlayElement("Core/BestFps");
        Ogre::OverlayElement* guiWorst = Ogre::OverlayManager::getSingleton().getOverlayElement("Core/WorstFps");

        const Ogre::RenderTarget::FrameStats& stats = mWindow->getStatistics();

        guiAvg->setCaption(avgFps + Ogre::StringConverter::toString(stats.avgFPS));
        guiCurr->setCaption(currFps + Ogre::StringConverter::toString(stats.lastFPS));
        guiBest->setCaption(bestFps + Ogre::StringConverter::toString(stats.bestFPS)
                            +" "+Ogre::StringConverter::toString(stats.bestFrameTime)+" ms");
        guiWorst->setCaption(worstFps + Ogre::StringConverter::toString(stats.worstFPS)
                             +" "+Ogre::StringConverter::toString(stats.worstFrameTime)+" ms");

        Ogre::OverlayElement* guiTris = Ogre::OverlayManager::getSingleton().getOverlayElement("Core/NumTris");
        guiTris->setCaption(tris + Ogre::StringConverter::toString(stats.triangleCount));

        Ogre::OverlayElement* guiDbg = Ogre::OverlayManager::getSingleton().getOverlayElement("Core/DebugText");
        guiDbg->show();
    }

    catch (...)
    {
    }

    return true;
}

void FrameListener::setCamera (TPCamera *cam)
{
    mExCamera = cam;
}
