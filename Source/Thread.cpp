
/*
 *
 *
 *
 *
 *
 *
 */

#include "Thread.h"
#include "ThreadGroup.h"
#include <process.h>


namespace ONE
{
	Thread::Thread()
	: m_Priority(PRIORITY_NORMAL)
	{
	}

	Thread::Thread(ThreadGroup* group)
	: m_Priority(PRIORITY_NORMAL)
	, m_pThreadGroup(group)
	{
		m_pThreadGroup->addThread(this);
	}

	Thread::~Thread()
	{
		if (m_pThreadGroup)
		{
			m_pThreadGroup->removeThread(this);
		}

		Close();
	}

	void Thread::Start()
	{
		#ifdef _WIN32
			if ((m_hThread = (HANDLE) _beginthreadex(NULL, 0, Entry, this, 0, &m_idThread)) < 0)
			{
				throw Exception("Thread creation failed");
			}
			else
			{
			    printf("ONE::Thread - Criada\n");
			}
		#else
			pthread_attr_t attr;

			pthread_attr_init(&attr);
			pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

			if (pthread_create(&m_thread, &attr, StartThread, this) == -1)
			{
				throw Exception("Thread creation failed");
			}
		#endif
	}

	void Thread::Wait(unsigned int time)
	{
		#ifdef _WIN32
		WaitForSingleObject(m_hThread, time);
		#else

		#endif
	}

	void Thread::Close()
	{
		#ifdef _WIN32
		CloseHandle(m_hThread);
		#else

		#endif
	}

	void Thread::Suspend()
	{
		#ifdef _WIN32
		SuspendThread(m_hThread);
		#else

		#endif
	}

	void Thread::Resume()
	{
		#ifdef _WIN32
		ResumeThread(m_hThread);
		#else

		#endif
	}

	void Thread::setPriority(int priority)
	{
		if (m_Priority != priority)
		{
			m_Priority = priority;

			#ifdef _WIN32
			if (m_hThread)
			{
				if (SetThreadPriority(m_hThread, m_Priority) == 0)
					throw Exception("Cannot set thread priority");
			}
			#else

			#endif
		}
	}

	int Thread::getPriority() const
	{
		#ifdef _WIN32
		return GetThreadPriority(m_hThread);
		#else

		#endif
	}

	void Thread::setThreadGroup(ThreadGroup* group)
	{
		m_pThreadGroup->removeThread(this);
		m_pThreadGroup = group;
		m_pThreadGroup->addThread(this);
	}

	ThreadGroup* Thread::getThreadGroup() const
	{
		return m_pThreadGroup;
	}

	unsigned __stdcall Thread::Entry(void* pThread)
	{
		try
		{
			reinterpret_cast<Thread*>(pThread)->Run();
		}
		catch (...)
		{
			throw Exception("Fatal Error in Thread");
		}

		return 0;
	}
}
