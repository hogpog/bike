
/*
 *
 *
 *
 *
 *
 */

#ifndef _FrameListener_h
#define _FrameListener_h

#include <OgreFrameListener.h>
#include "InputManager.h"
#include "TPCamera.h"
#include "Manager.h"
#include "Player.hpp"
#include <SDL.h>
#include <sstream>



class FrameListener : public Ogre::FrameListener
{
	public:
		FrameListener(InputManager* InputMgr, Ogre::RenderWindow* wnd, std::vector<Player*> *_player);
		~FrameListener();

		bool frameStarted(const Ogre::FrameEvent& event);
		bool frameEnded(const Ogre::FrameEvent& event);
        void setCharacter (Ogre::SceneNode *charNode);
        void setCamera (TPCamera *cam);

	private:
        TPCamera *mExCamera;
		InputManager* mInputMgr;
		OIS::Mouse*    mMouse;
		Ogre::RenderWindow* mWindow;
		Ogre::Overlay* mDebugOverlay;
		float mRotX, mRotY;
		Ogre::Vector3 pos;
		Ogre::Radian lYaw;
		std::vector<Player*> *player;
		Direction dir;
		Ogre::Vector3 lastPosition, midPosition;
		int lastAngle;
		int lastAngleFromX;
		std::string lastString;
		Ogre::Quaternion rot;
		int angleTarget;
		int lastAngleTarget;
		float angleTemp;

};

#endif
