
/*
 *
 *
 *
 *
 *
 *
 */

#ifndef _Globals_h
#define _Globals_h

#if defined (WIN32) && (BUILD_DLL)
# define DLL_EXPORT __declspec(dllexport)
#else
# define DLL_EXPORT
#endif

#ifdef WIN32
# include <windows.h>
# define close closesocket
# define SOCK_ERR SOCKET_ERROR
typedef int socklen_t;
#else
# include <sys/types.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <netdb.h>
# include <unistd.h>
# include <string>
# include <arpa/inet.h>
# define SOCK_ERR (-1)
typedef int SOCKET;
#endif

#include "Exception.h"
#include "Assert.h"


namespace ONE
{
}

#endif
