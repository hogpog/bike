
/*
 *
 *
 *
 *
 *
 */

#ifndef _Application_h
#define _Application_h

#include <Ogre.h>
#include <OgreException.h>
#include <OgreSingleton.h>

#include "InputManager.h"
#include "FrameListener.h"
#include "LoadingBar.h"
#include "Manager.h"
#include "AnimModel.h"

#include <SDL.h>
#include <SDL_net.h>
#include "Send.hpp"
#include "Receiver.hpp"
#include "Process.hpp"
#include "Mutex.h"
#include "Player.hpp"




class Application
{
	public:
		Application();
		virtual ~Application();

		bool Init();
		bool Run();
		bool Shut();

	protected:
		virtual bool InitGraphicsEngine();
		virtual bool InitIOEngine();
		virtual bool InitSoundEngine();
		virtual bool InitPhysicsEngine();
		virtual bool InitResources();

		virtual void ShutGraphicsEngine();
		virtual void ShutIOEngine();
		virtual void ShutSoundEngine();
		virtual void ShutPhysicsEngine();
		virtual void ShutResources();

		virtual void createSceneManager();
		virtual void createCamera();
		virtual void createViewports();
		virtual void creatFrameListener();
		virtual void createResourceListener();

		virtual void createScene();
		virtual void destroyScene();

		//Ogre::Root* mRoot;
		Ogre::RenderWindow* mRenderWnd;
		Ogre::Viewport *mViewport;
		//Ogre::SceneManager* mSceneMgr;
		Ogre::Camera* mCamera;

		InputManager* mInputManager;
		FrameListener* mFrameListener;
		LoadingBar* mLoadingBar;
		private:
		ONE::Mutex *mutexSend;
        ONE::Mutex *mutexRecv;
        Send *send;
        CReceiver *recv;
        CProcess *proc;
        bool conectado;
};

#endif
