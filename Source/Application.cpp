
/*
 *
 *
 *
 *
 *
 */

#include "Application.h"

Application::Application()
: mRenderWnd(0)
, mViewport(0)
, mInputManager(0)
, mFrameListener(0)
{
}

Application::~Application()
{
}

bool Application::Init()
{
    if (!InitGraphicsEngine())
		return false;
	if (!InitResources())
		return false;
	if (!InitPhysicsEngine())
		return false;
	if (!InitSoundEngine())
		return false;
	if (!InitIOEngine())
		return false;
	return true;
}

bool Application::Run()
{
	mLoadingBar = new LoadingBar();
	mLoadingBar->start(mRenderWnd, 1, 1, 1);



	//Manager::getSceneMgr()->createLight("Test");
	mCamera = Manager::getSceneMgr()->createCamera("PlayerCam");
	mCamera->setPosition(Ogre::Vector3(0,0,500));
	mCamera->lookAt(Ogre::Vector3(0,0,-300));
	mCamera->setNearClipDistance(5);


	Manager::getSceneMgr()->clearSpecialCaseRenderQueues();
	Manager::getSceneMgr()->addSpecialCaseRenderQueue(Ogre::RENDER_QUEUE_OVERLAY);
	Manager::getSceneMgr()->setSpecialCaseRenderQueueMode(Ogre::SceneManager::SCRQM_INCLUDE);

	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	Ogre::ResourceGroupManager::getSingleton().loadResourceGroup(
			Ogre::ResourceGroupManager::getSingleton().getWorldResourceGroupName(), false, true);

	Manager::getSceneMgr()->clearSpecialCaseRenderQueues();
	Manager::getSceneMgr()->setSpecialCaseRenderQueueMode(Ogre::SceneManager::SCRQM_EXCLUDE);

    ///*********************Carrega objetos/player/mapa
	/*Ogre::Entity *ent;
    Ogre::SceneNode* headNode = Manager::getSceneMgr()->getRootSceneNode()->createChildSceneNode();
    ent = Manager::getSceneMgr()->createEntity("bracos", "Arms.mesh");
    headNode->attachObject(ent);*/
    Ogre::Light *mLights = Manager::getSceneMgr()->createLight("Light1");
    mLights->setPosition(-500,500,500);
    mLights->setDiffuseColour(1,1,1);
//    mLights->setAmbientColour(1,1,1);
    mLights->setSpecularColour(1,1,1);
	mLights->setVisible(true);

	Ogre::Light *mLights2 = Manager::getSceneMgr()->createLight("Light2");
    mLights2->setPosition(500,-500,0);
    mLights2->setDiffuseColour(1,1,1);
   // mLights2->setAmbientColour(1,1,1);
    mLights->setSpecularColour(1,1,1);
	mLights2->setVisible(true);

	Ogre::Light *mLights3 = Manager::getSceneMgr()->createLight("Light3");
    mLights3->setPosition(500,500,-500);
    mLights3->setDiffuseColour(1,1,1);
   // mLights2->setAmbientColour(1,1,1);
    mLights->setSpecularColour(1,1,1);
	mLights3->setVisible(true);




    /*Ogre::Entity *map;
    Ogre::SceneNode* mapNode = Manager::getSceneMgr()->getRootSceneNode()->createChildSceneNode();
    map = Manager::getSceneMgr()->createEntity("map", "Arena.mesh");
    mapNode->attachObject(map);

*/
        const Ogre::RenderSystemCapabilities* caps = Ogre::Root::getSingleton().getRenderSystem()->getCapabilities();
        if (!caps->hasCapability(Ogre::RSC_VERTEX_PROGRAM))
        {
            OGRE_EXCEPT(Ogre::Exception::ERR_NOT_IMPLEMENTED, "Your card does not support vertex programs, so cannot "
                "run this demo. Sorry!",
                "Dot3Bump::createScene");
        }
        if (!(caps->hasCapability(Ogre::RSC_FRAGMENT_PROGRAM)
			|| caps->hasCapability(Ogre::RSC_DOT3)) )
        {
            OGRE_EXCEPT(Ogre::Exception::ERR_NOT_IMPLEMENTED, "Your card does not support dot3 blending or fragment programs, so cannot "
                "run this demo. Sorry!",
                "Dot3Bump::createScene");
        }
    Ogre::Entity* mEntities;
    Ogre::String mEntityMeshes = "pistagame.mesh";

			Ogre::MeshPtr pMesh = Ogre::MeshManager::getSingleton().load(mEntityMeshes,
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                Ogre::HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY,
				Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY,
				true, true); //so we can still read it
            // Build tangent vectors, all our meshes use only 1 texture coordset
			// Note we can build into VES_TANGENT now (SM2+)
            unsigned short src, dest;
            if (!pMesh->suggestTangentVectorBuildParams(Ogre::VES_TANGENT, src, dest))
            {
                pMesh->buildTangentVectors(Ogre::VES_TANGENT, src, dest);
            }
            // Create entity
            mEntities = Manager::getSceneMgr()->createEntity("map",
                mEntityMeshes);
            // Attach to child of root node
            Ogre::SceneNode* mapNode = Manager::getSceneMgr()->getRootSceneNode()->createChildSceneNode();
    		mapNode->attachObject(mEntities);
    		//mapNode->setScale(10,10,10);

/*
    AnimModel *anim2 = new AnimModel("armor.mesh",
                           "luvas.mesh",
                           "bracos.mesh",
                           "pernas.mesh",
                           "botas.mesh",
                           "hips.mesh");*/
    ///************************************ fim player mapa
    ///*ExTendedCamera*///
	TPCamera *exCamera = new TPCamera ("ExtendedCamera", mCamera);
    ///*ExTendedCamera*///


	Ogre::Viewport* vp = Manager::getRoot()->getAutoCreatedWindow()->addViewport(mCamera);
	vp->setBackgroundColour(Ogre::ColourValue(0.5,0.5,0.5));
    std::vector<Player*> playerData;
    playerData.push_back(new Player());
	FrameListener* frameListener = new FrameListener(mInputManager, mRenderWnd, &playerData);
	///*ExTendedCamera*///
    //frameListener->setCharacter (anim->getNode());
    frameListener->setCamera (exCamera);
	///*ExTendedCamera*///
	Manager::getRoot()->addFrameListener(frameListener);
	// Alter the camera aspect ratio to match the viewport
//	mCamera->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Real(vp->getActualHeight()));

	// Set ambient light
	Manager::getSceneMgr()->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));

	// Create a light
	Ogre::Light* l = Manager::getSceneMgr()->createLight("MainLight");
	l->setPosition(20,80,50);

	mLoadingBar->finish();
	delete mLoadingBar;
	if (!SDL_Init(0))
            {
                // initialize SDL_net
                if (SDLNet_Init()==-1)
                {
                    //printf("SDLNet_Init: %s\n",SDLNet_GetError());
                    printf("Nao foi possivel iniciar a SDL_net.\n");
                    exit(0);
                }
            }
            else
            {
                //printf("SDL_Init: %s\n",SDL_GetError());
                printf("Nao foi possivel iniciar a SDL.\n");
                system("Pause");
                exit(0);
            }
            IPaddress ip;
            Uint16 port = 7121;
            TCPsocket sock;
            if (!SDLNet_ResolveHost(&ip,"localhost"/*"projetoarret.servegame.com"*/,port))
            {
                // open socket
                sock=SDLNet_TCP_Open(&ip);
                if (!sock)
                {
                    system("CLS");
                    printf("\n\nNao foi possivel conectar, por favor, tente mais tarde.\n\n");
                    //exit(0);
                }
                else
                {

                    mutexSend = new ONE::Mutex();
                    mutexRecv = new ONE::Mutex();
                    send = new Send(sock,playerData[0],mutexSend);
                    send->Start();
                    recv = new CReceiver(sock,playerData[0],mutexRecv);
                    recv->Start();
                    proc = new CProcess(mutexRecv,&playerData);
                    proc->Start();
                    conectado = true;
                    playerData[0]->connected = true;
                }
            }



	Manager::getRoot()->startRendering();
	return true;
}

bool Application::Shut()
{
	ShutResources();
	ShutPhysicsEngine();
	ShutSoundEngine();
	ShutIOEngine();
	ShutGraphicsEngine();

	return true;
}

bool Application::InitGraphicsEngine()
{
	//Manager::getRoot() = new Ogre::Root("Plugins.cfg", "Twisted.cfg", "Graphicslog.txt");
	//Manager *mgr = new Manager;

	if (!Manager::getRoot())
	{
		return false;
	}

	if (!Manager::getRoot()->restoreConfig())
	{
		if (!Manager::getRoot()->showConfigDialog())
		{
			return false;
		}
	}

	mRenderWnd = Manager::getRoot()->initialise(true, "Game");

	return true;
}

bool Application::InitIOEngine()
{
	mInputManager = InputManager::getSingletonPtr();

	if (!mInputManager)
	{
		return false;
	}

	mInputManager->Init(mRenderWnd);

	return true;
}

bool Application::InitSoundEngine()
{
	return true;
}

bool Application::InitPhysicsEngine()
{
	return true;
}

bool Application::InitResources()
{
	Ogre::ConfigFile cf;
	cf.load("Resources.cfg");

	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
	while (seci.hasMoreElements())
	{
		Ogre::String secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
		for (Ogre::ConfigFile::SettingsMultiMap::iterator i = settings->begin();
			 i != settings->end(); ++i)
		{
			Ogre::String typeName = i->first;
			Ogre::String archName = i->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
		}
	}

	//Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	return true;
}

void Application::ShutGraphicsEngine()
{
	if (mRenderWnd) mRenderWnd->destroy();
}

void Application::ShutIOEngine()
{
	mInputManager->Quit();

	if (mInputManager) {
		delete mInputManager;
	}
}

void Application::ShutSoundEngine()
{
}

void Application::ShutPhysicsEngine()
{
}

void Application::ShutResources()
{
}

void Application::createSceneManager()
{
}

void Application::createCamera()
{
}

void Application::createViewports()
{
}

void Application::creatFrameListener()
{
	//mFrameListener = new FrameListener(mInputManager, mRenderWnd);
	//Manager::getRoot()->addFrameListener(mFrameListener);
}

void Application::createResourceListener()
{
}

void Application::createScene()
{
}

void Application::destroyScene()
{
}
