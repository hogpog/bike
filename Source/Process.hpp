#ifndef _PROCESS_HPP_
#define _PROCESS_HPP_
#include "Thread.h"
#include "Mutex.h"
#include <string>
#include <deque>
#include <vector>
#include <SDL.h>


class CProcess:public ONE::Thread
{
public:
    CProcess(ONE::Mutex *_mutex, std::vector<Player*> *_player)
    {
        player = _player;
        mutex = _mutex;

    };
    ~CProcess(){};
    void Run()
    {
        while (1)
        {
            if ((*player)[0]->bufferReceiver.size()>0)
            {
                std::string ProtocolAnaliserString = (*player)[0]->bufferReceiver[0];
                mutex->Lock();
                (*player)[0]->bufferReceiver.pop_front();
                mutex->Unlock();

                int IndProc = 0;

                std::string strProcTemp;
                strProcTemp = ProtocolAnaliserString[IndProc];
                std::string ID = "";
                std::string posX = "";
                std::string posY = "";
                std::string posZ = "";

                while (strProcTemp == "I")
                {
                    IndProc++;
                    ID += ProtocolAnaliserString[IndProc];

                    if (ProtocolAnaliserString[IndProc+1] == 'X'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                            ProtocolAnaliserString[IndProc+1] == 'Z'|| ProtocolAnaliserString[IndProc+1] == 'I'||
                            ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'S'||
                            ProtocolAnaliserString[IndProc+1] == 'A'|| ProtocolAnaliserString[IndProc+1] == 'L'||
                            ProtocolAnaliserString[IndProc+1] == '\0')
                    {
                        bool encontrou = false;
                        for (int C = 0; C < (*player).size(); C++)
                        {
                            if ((*player)[C]->id == atoi(ID.data()))
                            {
                                encontrou = true;
                                break;
                            }
                        }
                        if (!encontrou)
                        {
                            player->push_back(new Player());
                            (*player)[player->size()-1]->id = atoi(ID.data());
                        }
                        IndProc++;
                        strProcTemp = ProtocolAnaliserString[IndProc];
                        //(*player)[_ID]->AngleY = atoi(AngleY.data());
                    }
                }

                std::string AngleY = "";
                while (strProcTemp == "A")
                {
                    IndProc++;
                    AngleY += ProtocolAnaliserString[IndProc];

                    if (ProtocolAnaliserString[IndProc+1] == 'X'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                            ProtocolAnaliserString[IndProc+1] == 'Z'|| ProtocolAnaliserString[IndProc+1] == 'I'||
                            ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'S'||
                            ProtocolAnaliserString[IndProc+1] == 'A'|| ProtocolAnaliserString[IndProc+1] == 'L'||
                            ProtocolAnaliserString[IndProc+1] == '\0'|| ProtocolAnaliserString[IndProc+1] == 'a')
                    {
                        strProcTemp = ProtocolAnaliserString[IndProc+1];
                        for (int C = 0; C < (*player).size(); C++)
                        {
                            if ((*player)[C]->id == atoi(ID.data())&&C>0)
                            {
                                (*player)[C]->AngleY = -atoi(AngleY.data())-90;
                                break;
                            }
                        }
                        IndProc++;
                    }

                }
                std::string AngleX = "";
                while (strProcTemp == "a")
                {

                    IndProc++;
                    AngleX += ProtocolAnaliserString[IndProc];

                    if (ProtocolAnaliserString[IndProc+1] == 'X'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                            ProtocolAnaliserString[IndProc+1] == 'Z'|| ProtocolAnaliserString[IndProc+1] == 'I'||
                            ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'S'||
                            ProtocolAnaliserString[IndProc+1] == 'A'|| ProtocolAnaliserString[IndProc+1] == 'L'||
                            ProtocolAnaliserString[IndProc+1] == '\0'|| ProtocolAnaliserString[IndProc+1] == 'a')
                    {
                        strProcTemp = ProtocolAnaliserString[IndProc+1];
                        for (int C = 0; C < (*player).size(); C++)
                        {
                            if ((*player)[C]->id == atoi(ID.data()))
                            {
                                (*player)[0]->AngleX = atoi(AngleX.data());
                                //printf("foi azinho = %d\n",(*player)[0]->AngleX);
                                break;
                            }
                        }
                        IndProc++;
                    }

                }
                while (strProcTemp == "X")
                {
                    IndProc++;
                    posX += ProtocolAnaliserString[IndProc];

                    if (ProtocolAnaliserString[IndProc+1] == 'X'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                            ProtocolAnaliserString[IndProc+1] == 'Z'|| ProtocolAnaliserString[IndProc+1] == 'I'||
                            ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'S'||
                            ProtocolAnaliserString[IndProc+1] == 'A'|| ProtocolAnaliserString[IndProc+1] == 'L'||
                            ProtocolAnaliserString[IndProc+1] == '\0'|| ProtocolAnaliserString[IndProc+1] == 'a')
                    {
                        strProcTemp = ProtocolAnaliserString[IndProc+1];
                        for (int C = 0; C < (*player).size(); C++)
                        {
                            if ((*player)[C]->id == atoi(ID.data()))
                            {
                                (*player)[C]->Position.x = atoi(posX.data());
                                break;
                            }
                        }

                        IndProc++;
                        //printf("PosicaoX(%s) = %.2f",posX.data(),player->Position.X);
                    }
                }
                while (strProcTemp == "Y")
                {
                    IndProc++;
                    posY += ProtocolAnaliserString[IndProc];

                    if (ProtocolAnaliserString[IndProc+1] == 'X'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                            ProtocolAnaliserString[IndProc+1] == 'Z'|| ProtocolAnaliserString[IndProc+1] == 'I'||
                            ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'S'||
                            ProtocolAnaliserString[IndProc+1] == 'A'|| ProtocolAnaliserString[IndProc+1] == 'L'||
                            ProtocolAnaliserString[IndProc+1] == '\0'|| ProtocolAnaliserString[IndProc+1] == 'a')
                    {
                        strProcTemp = ProtocolAnaliserString[IndProc+1];
                        for (int C = 0; C < (*player).size(); C++)
                        {
                            if ((*player)[C]->id == atoi(ID.data()))
                            {
                                (*player)[C]->Position.y = atoi(posY.data());
                                //(*player)[C]->Position.Y += 22;
                                break;
                            }
                        }

                        IndProc++;
                        //printf("PosicaoY(%s) = %.2f",posY.data(),player->Position.Y);
                    }
                }
                while (strProcTemp == "Z")
                {
                    IndProc++;
                    posZ += ProtocolAnaliserString[IndProc];

                    if (ProtocolAnaliserString[IndProc+1] == 'X'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                            ProtocolAnaliserString[IndProc+1] == 'Z'|| ProtocolAnaliserString[IndProc+1] == 'I'||
                            ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'S'||
                            ProtocolAnaliserString[IndProc+1] == 'A'|| ProtocolAnaliserString[IndProc+1] == 'L'||
                            ProtocolAnaliserString[IndProc+1] == '\0'|| ProtocolAnaliserString[IndProc+1] == 'a')
                    {
                        strProcTemp = ProtocolAnaliserString[IndProc+1];
                        for (int C = 0; C < (*player).size(); C++)
                        {
                            if ((*player)[C]->id == atoi(ID.data()))
                            {
                                (*player)[C]->Position.z = atoi(posZ.data());
                                break;
                            }
                        }

                        IndProc++;
                        //printf("PosicaoZ(%s) = %.2f",posZ.data(),player->Position.Z);
                    }
                }
                while (strProcTemp == "F")
                {
                    for (int C = 0; C < (*player).size(); C++)
                    {
                        if ((*player)[C]->id == atoi(ID.data()))
                        {
                            (*player)[C]->Forward = true;
                            break;
                        }
                    }
                    if (ProtocolAnaliserString[IndProc+1] == 'X'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                            ProtocolAnaliserString[IndProc+1] == 'Z'|| ProtocolAnaliserString[IndProc+1] == 'I'||
                            ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'S'||
                            ProtocolAnaliserString[IndProc+1] == 'A'|| ProtocolAnaliserString[IndProc+1] == 'L'||
                            ProtocolAnaliserString[IndProc+1] == '\0'|| ProtocolAnaliserString[IndProc+1] == 'a')
                    {
                        strProcTemp = ProtocolAnaliserString[IndProc+1];

                        IndProc++;
                        //printf("PosicaoZ(%s) = %.2f",posZ.data(),player->Position.Z);
                    }
                }
                while (strProcTemp == "S")
                {
                    for (int C = 0; C < (*player).size(); C++)
                    {
                        if ((*player)[C]->id == atoi(ID.data()))
                        {
                            (*player)[C]->Forward = false;
                            break;
                        }
                    }
                    if (ProtocolAnaliserString[IndProc+1] == 'X'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                            ProtocolAnaliserString[IndProc+1] == 'Z'|| ProtocolAnaliserString[IndProc+1] == 'I'||
                            ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'S'||
                            ProtocolAnaliserString[IndProc+1] == 'A'|| ProtocolAnaliserString[IndProc+1] == 'L'||
                            ProtocolAnaliserString[IndProc+1] == '\0'|| ProtocolAnaliserString[IndProc+1] == 'a')
                    {
                        strProcTemp = ProtocolAnaliserString[IndProc+1];

                        IndProc++;
                        //printf("PosicaoZ(%s) = %.2f",posZ.data(),player->Position.Z);
                    }
                }
                std::string Name = "";
                while (strProcTemp == "L")
                {
                    IndProc++;
                    Name += ProtocolAnaliserString[IndProc];

                    if (ProtocolAnaliserString[IndProc+1] == '"'|| ProtocolAnaliserString[IndProc+1] == '\0')
                    {
                        for (int C = 0; C < (*player).size(); C++)
                        {
                            if ((*player)[C]->id == atoi(ID.data())&&C>0)
                            {
                                (*player)[C]->PlayerName = Name;
                                break;
                            }
                        }
                        strProcTemp = ProtocolAnaliserString[IndProc+1];
                        if (ProtocolAnaliserString[IndProc+1] != '\0')
                        {
                            IndProc++;
                            if (ProtocolAnaliserString[IndProc+1] == 'C')
                                strProcTemp = ProtocolAnaliserString[IndProc+1];
                        }
                    }
                }
                std::string chat = "";
                IndProc++;
                while (strProcTemp == "C")
                {
                    IndProc++;
                    chat += ProtocolAnaliserString[IndProc];
                    if (ProtocolAnaliserString[IndProc+1] == '\0')
                    {
                        strProcTemp = '\0';
                        for (int C = 0; C < (*player).size(); C++)
                        {
                            if ((*player)[C]->id == atoi(ID.data())&&C>0&&chat.size()>0)
                            {
                                (*player)[C]->stringChat.push_back(chat);
                                break;
                            }
                        }
                        printf("chat: %s\n",chat.c_str());
                        break;
                    }
                }

            }
            SDL_Delay(1);
        }
    };

private:
    ONE::Mutex *mutex;
    std::vector<Player*> *player;
};

#endif

