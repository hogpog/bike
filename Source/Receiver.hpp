#ifndef _RECEIVER_HPP_
#define _RECEIVER_HPP_
#include "Thread.h"
#include "player.hpp"
#include <string>
#include <deque>
#include <SDL_net.h>
#include "Mutex.h"

class CReceiver:public ONE::Thread
{
	public:
		CReceiver(TCPsocket &_ClientSocket,Player * _player,ONE::Mutex *_mutex);
		void newCReceiver(TCPsocket &_ClientSocket,Player * _player,ONE::Mutex *_mutex);
		~CReceiver();
        void Run();
         //Pega o ponteiro e mutex para o log
        void getLog(std::deque<std::string> *_log, ONE::Mutex *_logMutex);
	private:
        ONE::Mutex *mutex;
        Player *player;
        int LenCli;
        TCPsocket ClientSocket;
        //Log
        std::deque<std::string> *log;
        //Mutex para o log
        ONE::Mutex *logMutex;

};


#endif
