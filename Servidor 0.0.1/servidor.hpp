#ifndef _SERVIDOR_HPP_
#define _SERVIDOR_HPP_

//#include <gtkmm.h>
#include "util.hpp"
#include "Server.hpp"
#include "Thread.h"
#include "Mutex.h"

class CServerWindow /*: public Gtk::Window*/
{
public:
    CServerWindow();
    virtual ~CServerWindow();

protected:
//    CUtil util;
    CServer *server;
    void start();
    void pause();
    void stop();

    bool updateLog();

    //Logs;
    std::deque<std::string> log;
    //Mutex para o log
    Mutex *logMutex;
    //Botoes
    //Gtk::Button startButton;
    //GUI log
    //Gtk::ScrolledWindow scrolledWindow;
    //Gtk::TextView textView;
    //Glib::RefPtr<Gtk::TextBuffer> refTextBuffer;

};

#endif //GTKMM_EXAMPLEWINDOW_H

