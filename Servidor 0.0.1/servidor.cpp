#include "servidor.hpp"
//#include <gtkmm/stock.h>
//#include <iostream>

CServerWindow::CServerWindow()
{
    logMutex = new ONE::Mutex();
/*
    //util = new CUtil();
    //Cria uma janela
    Gtk::Window window;
    //Altera o t�tulo
    window.set_title("Servidor Arret 0.0.1");
    //Seta a largura da borda
    window.set_border_width(10);
    window.set_size_request(300, 400);
    window.set_resizable(false);
    //maximiza
    //window.maximize();

    //Cria um box vertical e adiciona na janela
    Gtk::VBox vbox;
    window.add(vbox);

    //cria um box horizontal e adiciona no box vertical
    Gtk::HBox hbox;
    vbox.pack_start(hbox,false,false,0);
    /// Num HBox tudo que se vai adicionando � verticalmente (seja botoes ou outros box)
    /// Num VBox tudo que se vai adicionando � horizontalmente (seja botoes ou outros box)
    /// Entao adicionei um VBox na tela, e dentro do VBox adicionei um HBox, e dentro do
    /// HBox eu adicionei os botoes;
    /// Se eu adicionar mais um HBox no VBox ele criaria em baixo dos botoes, e se eu
    /// for adicionando botoes nesse outro HBox ficaria uma linha de botoes em baixo da
    /// linha de botoes q ta em cima...(se nao deu pra entender me pergunta depois)
    //cria o botao start
    startButton.set_label("Start");
    hbox.pack_start(startButton,false,false,0);
    //linka o botao com um metodo
    startButton.signal_clicked().connect(sigc::mem_fun(this,
                                         &CServerWindow::start));
    //cria o botao pause
    Gtk::Button pauseButton("Pause");
    hbox.pack_start(pauseButton,false,false,0);
    pauseButton.set_sensitive(false);
    //linka o botao com um metodo
    pauseButton.signal_clicked().connect(sigc::mem_fun(this,
                                         &CServerWindow::pause));
    //cria o botao stop
    Gtk::Button stopButton("Stop");
    hbox.pack_start(stopButton,false,false,0);
    stopButton.set_sensitive(false);
    //linka o botao com um metodo
    stopButton.signal_clicked().connect(sigc::mem_fun(this,
                                        &CServerWindow::stop));
    //Add the TreeView, inside a ScrolledWindow, with the button underneath:
    scrolledWindow.add(textView);

    //Only show the scrollbars when they are necessary:
    scrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    textView.set_editable(false);
    textView.set_cursor_visible(false);
    Gtk::VBox vBoxLog;
    vBoxLog.pack_start(scrolledWindow);
    vbox.pack_start(vBoxLog,false,false,0);
    refTextBuffer = Gtk::TextBuffer::create();
    //mostra os itens adicionado na janela
    window.show_all();
    //roda a janela
    Glib::signal_timeout().connect( sigc::mem_fun(*this, &CServerWindow::updateLog),
                                    500 );
    log.push_back("GUI Iniciada!\n");
    //log.push_back("GUI Iniciada2!\n");

    Gtk::Main::run(window);*/
    start();
    while(1)SDL_Delay(1);
}

CServerWindow::~CServerWindow()
{
    //delete util;
}

void CServerWindow::start()
{
    //inicia o servidor
    server = new CServer();
    server->getLog(&log,logMutex);
//    startButton.set_sensitive(false);
    server->Start();
}

void CServerWindow::pause()
{
    //pausa o servidor
    server->Pause();
}

void CServerWindow::stop()
{
    //para o servidor(ainda nao funciona oO)
    server->Stop();
    //exit(0);
    //delete server;
}

bool CServerWindow::updateLog()
{
    logMutex->Lock();
    for(int numLog = 0; numLog < log.size();numLog++)
    {
//        Gtk::TextBuffer::iterator itLog;
//        itLog = refTextBuffer->get_iter_at_offset(0);
        //itLog.forward_to_end();
//        itLog.backward_line();
//        refTextBuffer->place_cursor(itLog);
//        refTextBuffer->insert_at_cursor(log[0]);
//        textView.set_buffer(refTextBuffer);
        printf("%s",log[0].data());
        log.pop_front();
    }
    logMutex->Unlock();
    return true;
}

