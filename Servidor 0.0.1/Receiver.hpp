#ifndef _RECEIVER_HPP_
#define _RECEIVER_HPP_
#include "Thread.h"
#include "player.hpp"
#include <string>
#include <deque>
#include <SDL_net.h>
#include "Mutex.h"
#include <boost/shared_ptr.hpp>
#include "types.hpp"


class CReceiver:public ONE::Thread
{
	public:
		CReceiver(TCPsocket &_ClientSocket, Player *_player, Mutex *_mutex);
		void newCReceiver(TCPsocket &_ClientSocket, Player *_player,Mutex *_mutex);
		~CReceiver();
        void Run();
         //Pega o ponteiro e mutex para o log
        void getLog(std::deque<std::string> *_log, Mutex *_logMutex);
	private:
        Mutex *mutex;
        Player *player;
        int LenCli;
        TCPsocket ClientSocket;
        //Log
        std::deque<std::string> *log;
        //Mutex para o log
        Mutex *logMutex;

};


#endif
