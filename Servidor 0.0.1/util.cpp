#include"util.hpp"

//Transforma string normal pra UTF-8
/*Glib::ustring utf(std::string str)
{
    return Glib::locale_to_utf8(str.c_str());
}

CUtil::CUtil()
{
    iterator = 0;
}
CUtil::~CUtil()
{
    for (unsigned int size=0;size<label.size();size++)
    {
        delete label[size];
    }
    for (unsigned int size=0;size<vbox.size();size++)
    {
        delete vbox[size];
    }
    label.clear();
    vbox.clear();
}
Glib::ustring CUtil::utf(std::string str)
{
    return Glib::locale_to_utf8(str.c_str());
}

std::string CUtil::tostr(Glib::ustring txt)
{
    return txt;
}

std::string CUtil::tostr(int num)
{
    std::stringstream ss;
    std::string str;
    ss << num;
    ss >> str;
    return str;
}

void CUtil::CriaCampo(Gtk::Entry &Entry, std::string nomecampo,Gtk::VBox &box)
{
    vbox.push_back(new Gtk::VBox);
    label.push_back(new Gtk::Label);
    box.pack_start(*vbox[iterator],false,false,1);
    vbox[iterator]->show();

    vbox[iterator]->pack_start(*label[iterator],false,false,1);
    label[iterator]->set_alignment(Gtk::JUSTIFY_LEFT);
    //label[iterator]->set_label(utf(nomecampo));
    label[iterator]->set_markup("<b>"+utf(nomecampo)+"</b>");
    label[iterator]->show();

    vbox[iterator]->pack_start(Entry,false,false,1);
    Entry.show();
    iterator++;
}
void CUtil::CriaCampo(Gtk::Entry &Entry, std::string nomecampo,Gtk::HBox &box)
{
    vbox.push_back(new Gtk::VBox);
    label.push_back(new Gtk::Label);
    box.pack_start(*vbox[iterator],false,false,1);
    vbox[iterator]->show();

    vbox[iterator]->pack_start(*label[iterator],false,false,1);
    label[iterator]->set_alignment(Gtk::JUSTIFY_LEFT);
    //label[iterator]->set_label(utf(nomecampo));
    label[iterator]->set_markup("<b>"+utf(nomecampo)+"</b>");
    label[iterator]->show();

    vbox[iterator]->pack_start(Entry,false,false,0);
    Entry.show();
    iterator++;
}

void CUtil::CriaCampoFinal(Gtk::Entry &Entry, std::string nomecampo,Gtk::HBox &box)
{
    vbox.push_back(new Gtk::VBox);
    label.push_back(new Gtk::Label);
    box.pack_end(*vbox[iterator],false,false,1);
    vbox[iterator]->show();

    vbox[iterator]->pack_start(*label[iterator],false,false,1);
    label[iterator]->set_alignment(Gtk::JUSTIFY_LEFT);
    //label[iterator]->set_label(utf(nomecampo));
    label[iterator]->set_markup("<b>"+utf(nomecampo)+"</b>");
    label[iterator]->show();

    vbox[iterator]->pack_end(Entry,false,false,0);
    Entry.show();
    iterator++;
}
void CUtil::InitMenus(Glib::ustring &strg,Glib::RefPtr<Gtk::ActionGroup> m_refActionGroup)
{
    menus = m_refActionGroup;
    str = &strg;
    *str+="<ui>";
    *str+="<menubar name='MenuBar'>";
}
void CUtil::EndMenu()
{
    *str+= "</menu>";
}
void CUtil::Separator()
{
    *str+="<separator/>";
}
void CUtil::MenuAdd(std::string nome, std::string id)
{
    *str+="<menu action='";
    *str+=id;
    *str+="'>";
    menus->add(Gtk::Action::create(id, utf(nome)));
}

void CUtil::ItemAdd(std::string id)
{
    *str+="<menuitem action='"+id+"'/>";

//    m_refActionGroup->add(Gtk::Action::create(nome+"Item",
//                          "_"+nome, nome),
//                          sigc::mem_fun(*this,&(reinterpret_cast<class>(*metodo))));
}

void CUtil::Uppercase(Gtk::Entry &Entry)
{
    Glib::ustring toup = Entry.get_text();
    toup.uppercase();
    Entry.set_text(toup);
}

void CUtil::Uppercase(std::string &str)
{
    std::transform (str.begin(), str.end(), str.begin(), toupper);
}

*/
