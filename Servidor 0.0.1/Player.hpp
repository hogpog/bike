#ifndef _PLAYER_HPP_
#define _PLAYER_HPP_

#include "deque"
#include <string>
#include <sstream>
#include <irrlicht.h>

class Player
{
	public:
        unsigned int id;
        bool connected;
        bool Foward;
        std::string PlayerName;
        std::deque<std::string> bufferReceiver;
        std::deque<std::string> bufferSend;
        ///Chat
        std::string stringChat;
        ///Msg que ser� enviada ao cliente
        std::string stringToSend;
        std::string LstringToSend;
        ///Angulo
        int AngleY;
        short int AngleX;
        ///Posi��es
        irr::core::vector3df Position;
		Player(){Position.X=0;Position.Y=0;Position.Z=0;AngleY=0;Foward=false;jump=false;};
		~Player(){};
		bool jump;
		///Zera Variaveis
		void ZeraVariaveis(){
            id = 0;
            connected = false;
            Foward = false;
            PlayerName.clear();
            bufferReceiver.clear();
            bufferSend.clear();
            ///Msg que ser� enviada ao cliente
            stringToSend.clear();
            LstringToSend.clear();
            ///Angulo
            AngleY = 0;
            AngleX = 0;
            ///Posi��es
            Position = (irr::core::vector3df(0,0,0));
        };
	protected:

	private:

};


#endif
