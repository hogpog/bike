#include "Physics.hpp"

CPhysics::CPhysics(std::vector<Player*> *_player)
{
    srand ( time(NULL) );

    lineDown.start = core::vector3df(0,0,0);
    lineDown.start.Y += 100;
    lineDown.end = core::vector3df(0,0,0);
    lineDown.end.Y -= 5;

    isAddPlayer = false;
    device =(createDevice(EDT_NULL));
    smgr = (device->getSceneManager());
    driver = (device->getVideoDriver());
    //device->getFileSystem()->addZipFileArchive("media/data.pk3");
    levelMesh = (smgr->getMesh("media/pistagame.mesh"));

    levelNode = (smgr->addOctTreeSceneNode(levelMesh->getMesh(0)));
    levelNode->setRotation(vector3df(0,0,0));
    levelNode->setMaterialTexture( 0, driver->getTexture("media/terrain-texture.jpg"));
    levelNode->setMaterialFlag(EMF_LIGHTING, false);
    selector = smgr->createTriangleSelector(smgr->getMesh("media/pistagame.mesh")->getMesh(0),levelNode);
    levelNode->setTriangleSelector(selector);
    //levelNode->setPosition(vector3df(0,-100,0));
    //levelNode->setScale(vector3df(10,10,10));


    /* Because the level was modelled not around the origin (0,0,0), we translate
    the whole level a little bit. */
    //levelNode->setPosition(core::vector3df(0,-3500,0));

    p_world = (createPhysicsWorld(device));

    //create level tree
    SBodyFromNode worldData;
    worldData.Node = levelNode;
    worldData.Mesh = levelMesh->getMesh(0);

    //worldData.TerrainLOD = 5;
    world_p_node = ((irr::newton::ITreeBody*) p_world->createBody(worldData));

    rigell_material = (p_world->createMaterial());
    level_material  = (p_world->createMaterial());

    rigell_material->setElasticity(level_material, 0.1f);
    rigell_material->setFriction(level_material, 0.1f,0.01f);
    rigell_material->setSoftness(level_material, 0.20f);
    rigell_material->setCollidable(rigell_material,false);


    player = _player;

    //scene::ICameraSceneNode *cam = smgr->addCameraSceneNode();
    cam = (smgr->addCameraSceneNodeMaya());
    cam->setPosition(core::vector3df(0,5,0));
    //cam->setFarValue(10000);
    addPlayer();
    //for (int numPhysic=0;numPhysic<50; numPhysic++)
    //addPlayer();


}

CPhysics::~CPhysics()
{
}

void CPhysics::Run()
{
    Uint32 initTime = -1;
    Uint32 endTime=0;
    Uint32 diffTime=0;
    Uint32 sumTime=0;

    while (1)
    {
        device->run();
        /*cam->setTarget(rigell_body[0]->getPosition());
        cam->setPosition(core::vector3df(rigell_body[0]->getPosition().X,
                                         rigell_body[0]->getPosition().Y+150,
                                         rigell_body[0]->getPosition().Z+150));*/
        /*endTime = initTime;
        initTime = SDL_GetTicks();
        diffTime = initTime-endTime;
        sumTime += diffTime;
        times++;
        if(sumTime>=1000)
        {
            printf("FPS: %d\n",times);
            sumTime = 0;
            times = 0;
        }*/



        // ou printf("%d\n",fps);

        /*Frames++;
        int t = SDL_GetTicks();
        if (t - T0 >= 1000)
        {
            float seconds = (t - T0) / 1000.0;
            fps = Frames / seconds;
            T0 = t;
            Frames = 0;
            //printf("FPS: %d\n",fps);
        }*/
        playerDataToPhysic();

        //driver->beginScene(true, true, video::SColor(0,100,100,100));

        p_world->update();
        //smgr->drawAll();
        //pmgr->showCollision();
        //driver->endScene();
        //Draw();

        getPhysicResults();


        diffTime = SDL_GetTicks() - endTime;
        endTime = SDL_GetTicks();
        if (diffTime<33)
            SDL_Delay(33-diffTime);
    }
}
void CPhysics::Draw()
{
    driver->beginScene(true, true, video::SColor(0,100,100,100));
    smgr->drawAll();
    driver->draw3DLine(lineDown.start,lineDown.end);
    p_world->drawAllDebugInfos();
    driver->endScene();
}
void CPhysics::playerDataToPhysic()
{
    for (int nPlayer = 0; nPlayer < player->size(); nPlayer++)
    {
        core::vector3df velocity = rigell_body[nPlayer]->getVelocity();
        velocity.X=velocity.Z=0;
        node[nPlayer]->setRotation(core::vector3df(0,-(*player)[nPlayer]->AngleY,0));


        /*lineFront.start = lineDown.start;
        lineFront.end.X = rigell_body[nPlayer]->getPosition().X+(50*cos((*player)[nPlayer]->AngleY));
        lineFront.end.Y = rigell_body[nPlayer]->getPosition().Y;
        lineFront.end.Z = rigell_body[nPlayer]->getPosition().Z+(50*sin((*player)[nPlayer]->AngleY));
        lineBack.start = lineDown.start;
        lineBack.end.X = rigell_body[nPlayer]->getPosition().X-(50*cos((*player)[nPlayer]->AngleY));
        lineBack.end.Y = rigell_body[nPlayer]->getPosition().Y;
        lineBack.end.Z = rigell_body[nPlayer]->getPosition().Z-(50*sin((*player)[nPlayer]->AngleY));*/

        if ((*player)[nPlayer]->Foward)
        {
            //rigell_body[rigell_body.size()-1]->setAutoFreeze(false);
            velocity += rigell_body[nPlayer]->FRIgetDirectionPositionY(
                            core::vector3df(0.5f,0,0));
            //velocity+=0.1;

        }
        else
        {
            //rigell_body[rigell_body.size()-1]->setAutoFreeze(true);
            //NewtonWorldFreezeBody(pmgr->getNewtonWorld(),pNode[nPlayer]->getNewtonBody());
            //pNode[nPlayer]->setVelocity(core::vector3df(0,0,0));
        }
        /*if(getCollisionPoint(
        		irr::newton::IBody* body,
        		irr::core::line3d<irr::f32> line,
        		irr::newton::SIntersectionPoint& out);
        	*/



        lineDown.start = (*player)[0]->Position;
        lineDown.start.Y += 100;
        lineDown.end = (*player)[0]->Position;
        lineDown.end.Y -= 3;

        if (p_world->getCollisionManager()->getCollisionPoint(world_p_node,lineDown,out))
            rigell_body[nPlayer]->setVelocity(velocity);
        if((*player)[nPlayer]->jump)
        {
            rigell_body[nPlayer]->jump(0.4);
            (*player)[nPlayer]->jump=false;
        }




        irr::core::vector3df intersection;
        irr::core::triangle3df triangle;
        irr::core::line3df line;
        line =  lineDown;

        if(smgr->getSceneCollisionManager()->getCollisionPoint(line,selector,intersection,triangle))
        {
            (*player)[nPlayer]->AngleX = triangle.getNormal().getHorizontalAngle().X;
        }
        /*lineFront.start.X = rigell_body[nPlayer]->getPosition().X+(cos((*player)[nPlayer]->AngleY*3.1415/180));
        lineFront.start.Y = rigell_body[nPlayer]->getPosition().Y+10;
        lineFront.start.Z = rigell_body[nPlayer]->getPosition().Z+(sin((*player)[nPlayer]->AngleY*3.1415/180));

        lineFront.end.X = rigell_body[nPlayer]->getPosition().X+(cos((*player)[nPlayer]->AngleY*3.1415/180));
        lineFront.end.Y = rigell_body[nPlayer]->getPosition().Y-10;
        lineFront.end.Z = rigell_body[nPlayer]->getPosition().Z+(sin((*player)[nPlayer]->AngleY*3.1415/180));

        lineBack.start.X = rigell_body[nPlayer]->getPosition().X-(cos((*player)[nPlayer]->AngleY*3.1415/180));
        lineBack.start.Y = rigell_body[nPlayer]->getPosition().Y+10;
        lineBack.start.Z = rigell_body[nPlayer]->getPosition().Z-(sin((*player)[nPlayer]->AngleY*3.1415/180));

        lineBack.end.X = rigell_body[nPlayer]->getPosition().X-(cos((*player)[nPlayer]->AngleY*3.1415/180));
        lineBack.end.Y = rigell_body[nPlayer]->getPosition().Y-10;
        lineBack.end.Z = rigell_body[nPlayer]->getPosition().Z-(sin((*player)[nPlayer]->AngleY*3.1415/180));
        core::vector3df inter1, inter2;
        if(smgr->getSceneCollisionManager()->getCollisionPoint(lineFront,selector,inter1,triangle))
        {
            if(smgr->getSceneCollisionManager()->getCollisionPoint(lineBack,selector,inter2,triangle))
            {
                (*player)[nPlayer]->AngleX = -atan2(inter2.Y, inter1.Y)*180/3.1415;
            }
        }*/

    }
}
void CPhysics::getPhysicResults()
{
    for (int nPlayer = 0; nPlayer < player->size(); nPlayer++)
    {
        if ((*player)[nPlayer]->connected)
        {
            //NewtonWorldUnfreezeBody(pmgr->getNewtonWorld(),pNode[nPlayer]->getNewtonBody());
            //pNode[nPlayer]->setMass(10);
            //NewtonBodySetAutoFreeze(pNode[nPlayer]->getNewtonBody(), 0);
            (*player)[nPlayer]->Position =  node[nPlayer]->getPosition();
//            printf("Player %d esta na posicao X:%.2f Y:%.2f Z:%.2f\n",nPlayer,
//                   (*player)[nPlayer]->Position.X,(*player)[nPlayer]->Position.Y,(*player)[nPlayer]->Position.Z);
        }
        else
        {
            node[nPlayer]->setPosition(core::vector3df(0,5,0));
            //NewtonWorldFreezeBody(pmgr->getNewtonWorld(),pNode[nPlayer]->getNewtonBody());
        }
    }
}
void CPhysics::addPlayer()
{

    //isAddPlayer = true;
    //rigell_body.push_back(NULL);
//    node.push_back(NULL);

    scene::IAnimatedMesh* rigell_mesh = smgr->getMesh("media/Bike.mesh");
    node.push_back((smgr->addMeshSceneNode(
                        rigell_mesh->getMesh(0))));

    //node[rigell_body.size()-1]->setScale(vector3df(0.9f,0.9f,0.9f));

    newton::SBodyFromNode bodyData;
    bodyData.Type = newton::EBT_PRIMITIVE_CAPSULE;
    bodyData.Node = node[node.size()-1];
//	bodyData.Mesh = rigell_mesh->getMesh(1);

    rigell_body.push_back(p_world->createCharacterController(
                              p_world->createBody(bodyData)));
    //rigell_body[rigell_body.size()-1]->setRotationUpdate(false);
    rigell_body[rigell_body.size()-1]->setContinuousCollisionMode(false);

    //the body rotate never
    p_world->getUtils()->avoidRotationOnAllAxes(rigell_body[rigell_body.size()-1]);

    rigell_body[rigell_body.size()-1]->setMaterial(rigell_material);
    world_p_node->setMaterial(level_material);


    // set the force to apply if in front of the character there is a stair
    rigell_body[rigell_body.size()-1]->setClimbStairForce(core::vector3df(0,0.01f,0));

    //set the max stair-step height from floor for the first stair-step
    //from the below stair-step for all the stairs-step except the first
    rigell_body[rigell_body.size()-1]->setStairMaxHeight( 0.01f );

    //add gravity
    rigell_body[rigell_body.size()-1]->addForceContinuous(core::vector3df(0,
            -1,
            0));
    //rigell_body[rigell_body.size()-1]->setFreezeTreshold(0.1f,0.1f,10);
    //rigell_body[rigell_body.size()-1]->setAutoFreeze(true);

    rigell_body[rigell_body.size()-1]->setPosition(core::vector3df(0,5,0));
    rigell_body[rigell_body.size()-1]->setMass(1);
    //pNode[pNode.size()-1]->setMass(0);
    //NewtonBodySetAutoFreeze(pNode[pNode.size()-1]->getNewtonBody(), 0);
    //NewtonBodySetAutoFreeze(pNode[pNode.size()-1]->getNewtonBody(), 1);
    //pNode[pNode.size()-1]->setMass(0);
    //pNode[pNode.size()-1]->setMass(0.1);
}
void CPhysics::toAddPlayer(int id)
{
    rigell_body[id]->setPosition(core::vector3df(0,5,0));
    /*if (isAddPlayer)
    {
        pNode.push_back(NULL);
        node.push_back(NULL);
        node[node.size()-1] = smgr->addSphereSceneNode();

        pNode[pNode.size()-1] = pmgr->addControllablePhysicsNode(node[node.size()-1],
                                tumle::EBT_CAPSULE,
                                1, core::vector2df(5,20),
                                core::vector3df(0,0,90));

        pNode[pNode.size()-1]->setCapForceAt(1500);
        pNode[pNode.size()-1]->setCapForceMode(true);
        pNode[pNode.size()-1]->setMaterial(nodeM);
        isAddPlayer = false;
    }*/
}
