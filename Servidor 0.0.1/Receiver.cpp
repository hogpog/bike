#include "Receiver.hpp"

CReceiver::CReceiver(TCPsocket &_ClientSocket, Player *_player,Mutex *_mutex)
{
    mutex = _mutex;
    ClientSocket = _ClientSocket;
    player = _player;

}
void CReceiver::newCReceiver(TCPsocket &_ClientSocket, Player *_player, Mutex *_mutex)
{
    mutex = _mutex;
    ClientSocket = _ClientSocket;
    player = _player;

}
CReceiver::~CReceiver()
{
}

void CReceiver::Run()
{
    char StrRecv[1024];
    while (1)
    {
        if (player->connected)
        {
            for(int C=0; C<1024; C++)
            {
                StrRecv[C]='\0';
            }
            LenCli=SDLNet_TCP_Recv(ClientSocket, StrRecv, 1024);
            mutex->Lock();
            printf("%s\n",StrRecv);
            player->bufferReceiver.push_back(StrRecv);
            if(LenCli<0&&player->connected)
            {
                char logtmp[100];
                sprintf(logtmp,"Cliente ID: %d desconectou\n", player->id);
                logMutex->Lock();
                log->push_back(logtmp);
                logMutex->Unlock();
                player->connected=false;
                SDLNet_TCP_Close(ClientSocket);
                player->ZeraVariaveis();
                break;
            }
            mutex->Unlock();
        }
        SDL_Delay(1);
    }
}

void CReceiver::getLog(std::deque<std::string> *_log, Mutex *_logMutex)
{
    log = _log;
    logMutex = _logMutex;
}
