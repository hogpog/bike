#ifndef _SEND_HPP
#define _SEND_HPP

#include "Thread.h"
#include "SDL_Net.h"
#include "player.hpp"
#include "types.hpp"


class Send:public ONE::Thread
{
	public:
		Send(TCPsocket &_ClientSocket, Player *_player, Mutex *_mutex);
		void newSend(TCPsocket &_ClientSocket, Player *_player, Mutex *_mutex);
		~Send();
		void Run();
	protected:

	private:
        Mutex *mutex;
        Player *player;
        int LenCli;
        TCPsocket ClientSocket;

};


#endif
