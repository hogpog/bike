
/*
 *
 *
 *
 *
 *
 *
 */

#ifndef _Mutex_h
#define _Mutex_h

#include "Globals.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <pthread.h>
#include <sys/types.h>
#endif



namespace ONE
{
	class DLL_EXPORT Mutex
	{
		public:
			Mutex();
			virtual ~Mutex();

			void Lock();
			void Unlock();

		private:
			Mutex(const Mutex &mutex) {};
			Mutex& operator=(const Mutex &mutex) { return *(this); };

			#ifdef _WIN32
				HANDLE m_hMutex;
			#else
				pthread_mutex_t m_hMutex;
			#endif
	};
}

#endif
