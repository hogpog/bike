#ifndef _TYPES_HPP_
#define _TYPES_HPP_

#include <newton.h>
#include <irrlicht.h>
#include <IrrNewt.hpp>
#include <SDL_net.h>
#include <boost/shared_ptr.hpp>
#include "Mutex.h"

using namespace irr;
using namespace scene;
using namespace video;
using namespace core;
using namespace newton;
using namespace ONE;

#endif
