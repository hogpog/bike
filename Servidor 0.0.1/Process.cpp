#include "Process.hpp"

Process::Process(std::vector<Player*> *_player, Mutex *_mutex[2],int _ID)
{
    player = _player;
    mutex[0]=_mutex[0];
    mutex[1]=_mutex[1];
    //ID = _ID;
}
void Process::newProcess(std::vector<Player*> *_player, Mutex *_mutex[2],int _ID)
{
    player = _player;
    mutex[0]=_mutex[0];
    mutex[1]=_mutex[1];
    //ID = _ID;
}

Process::~Process()
{}

void Process::Run()
{
    int psize;

    while (1)
    {
        psize = player->size();
        for (int ID = 0; ID < psize;ID++)
        {
            if ((*player)[ID]->connected)
            {
                (*player)[ID]->stringToSend="";
                mutex[0]->Lock();
                mutex[1]->Lock();
                if ((*player)[ID]->bufferReceiver.size()>0)
                {
                    ProtocolAnaliser(ID);
                    //printf("Recebe: %s\n",(*player)[_ID]->bufferReceiver[0].data());
                    (*player)[ID]->bufferReceiver.pop_front();
                }

                ///Processa os dados
                std::stringstream stringtmp;
                stringtmp.str() = "";
                stringtmp.clear();

                stringtmp<<"I"<<ID+1;
                stringtmp<<"A"<<(*player)[ID]->AngleY
                <<"a"<<(*player)[ID]->AngleX
                <<"X"<<(int)((*player)[ID]->Position.X*10)
                <<"Y"<<(int)((*player)[ID]->Position.Y*10)
                <<"Z"<<(int)((*player)[ID]->Position.Z*10);
                if ((*player)[ID]->Foward)
                {
                    stringtmp<<"F";
                }
                else
                {
                    stringtmp<<"S";
                }
                stringtmp<<"L"<<(*player)[ID]->PlayerName<<"\"";
                if ((*player)[ID]->stringChat.size()>0)
                {
                    stringtmp<<"C"<<(*player)[ID]->stringChat<<"\0";
                    (*player)[ID]->stringChat = "";
                }

                std::stringstream stringtmp2;
                stringtmp2.str() = "";
                stringtmp2.clear();

                stringtmp2<<"I"<<0;
                stringtmp2<<"A"<<(*player)[ID]->AngleY
                <<"a"<<(*player)[ID]->AngleX
                <<"X"<<(int)((*player)[ID]->Position.X*10)
                <<"Y"<<(int)((*player)[ID]->Position.Y*10)
                <<"Z"<<(int)((*player)[ID]->Position.Z*10);
                if ((*player)[ID]->Foward)
                {
                    stringtmp2<<"F";
                }
                else
                {
                    stringtmp2<<"S";
                }

                std::string stringToSend;
                stringToSend.clear();
                stringToSend = stringtmp.str();

                std::string stringToSend2;
                stringToSend2.clear();
                stringToSend2 = stringtmp2.str();
                ///Verifica Se mudou alguma coisa da ultima msg enviada.
                if (strcmp((*player)[ID]->LstringToSend.data(),stringToSend.data()))
                {
                    ///Verifica Se os jogadores est�o perto dele para enviar os dados
                    for (int NumPlayer = 0; NumPlayer < psize; NumPlayer++)
                    {
                        if ((*player)[NumPlayer]->Position.X>(*player)[ID]->Position.X-2000&&
                                (*player)[NumPlayer]->Position.X<(*player)[ID]->Position.X+2000&&
                                (*player)[NumPlayer]->Position.Z>(*player)[ID]->Position.Z-2000&&
                                (*player)[NumPlayer]->Position.Z<(*player)[ID]->Position.Z+2000&&
                                (*player)[NumPlayer]->connected)
                        {
                            ///Verifica se ele vai enviar a string pra ele mesmo, se for o ID vai 0
                            if (NumPlayer != ID)
                            {
                                (*player)[NumPlayer]->bufferSend.push_back(stringToSend);

                                //printf("%s\n",stringToSend.data());
                                //printf("string Gerada: %s\n",(*player)[0]->bufferSend[0].data());


                            }
                            else
                            {

                                (*player)[NumPlayer]->bufferSend.push_back(stringToSend2);

                            }
                        }
                    }
                    (*player)[ID]->LstringToSend=stringToSend;
                    (*player)[ID]->stringToSend.clear();
                }

                mutex[1]->Unlock();
                mutex[0]->Unlock();


            }
        }
        SDL_Delay(1);
    }
}

void Process::ProtocolAnaliser(int _ID)
{

    std::string ProtocolAnaliserString = (*player)[_ID]->bufferReceiver[0];

    int IndProc = 0;

    ///************************ID************************///

    std::string strProcTemp;
    strProcTemp = ProtocolAnaliserString[IndProc];
    std::string AngleY = "";

    while (strProcTemp == "A")
    {
        IndProc++;
        AngleY += ProtocolAnaliserString[IndProc];

        if (ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                ProtocolAnaliserString[IndProc+1] == 'S'|| ProtocolAnaliserString[IndProc+1] == 'A'||
                ProtocolAnaliserString[IndProc+1] == 'N'|| ProtocolAnaliserString[IndProc+1] == '\0' ||
                ProtocolAnaliserString[IndProc+1] == 'L' || ProtocolAnaliserString[IndProc+1] == 'C'||
                ProtocolAnaliserString[IndProc+1] == 'J' )
        {
            strProcTemp = ProtocolAnaliserString[IndProc+1];
            (*player)[_ID]->AngleY = atoi(AngleY.data());
            printf("angulo: %d\n",(*player)[_ID]->AngleY);
            (*player)[_ID]->AngleY;
        }

    }
    if (strProcTemp == "F")
    {
        (*player)[_ID]->Foward = true;
        IndProc++;

        if (ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                ProtocolAnaliserString[IndProc+1] == 'S'|| ProtocolAnaliserString[IndProc+1] == 'A'||
                ProtocolAnaliserString[IndProc+1] == 'N'|| ProtocolAnaliserString[IndProc+1] == '\0' ||
                ProtocolAnaliserString[IndProc+1] == 'L' || ProtocolAnaliserString[IndProc+1] == 'C'||
                ProtocolAnaliserString[IndProc+1] == 'J' )
        {
            strProcTemp = ProtocolAnaliserString[IndProc+1];
        }

    }
    if (strProcTemp == "S")
    {
        (*player)[_ID]->Foward = false;
        IndProc++;

        if (ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                ProtocolAnaliserString[IndProc+1] == 'S'|| ProtocolAnaliserString[IndProc+1] == 'A'||
                ProtocolAnaliserString[IndProc+1] == 'N'|| ProtocolAnaliserString[IndProc+1] == '\0' ||
                ProtocolAnaliserString[IndProc+1] == 'L' || ProtocolAnaliserString[IndProc+1] == 'C'||
                ProtocolAnaliserString[IndProc+1] == 'J' )
        {
            strProcTemp = ProtocolAnaliserString[IndProc+1];
        }

    }
    IndProc++;
    if (strProcTemp == "J")
    {
        (*player)[_ID]->jump = true;

        IndProc++;

        if (ProtocolAnaliserString[IndProc+1] == 'F'|| ProtocolAnaliserString[IndProc+1] == 'Y'||
                ProtocolAnaliserString[IndProc+1] == 'S'|| ProtocolAnaliserString[IndProc+1] == 'A'||
                ProtocolAnaliserString[IndProc+1] == 'N'|| ProtocolAnaliserString[IndProc+1] == '\0' ||
                ProtocolAnaliserString[IndProc+1] == 'L' || ProtocolAnaliserString[IndProc+1] == 'C'||
                ProtocolAnaliserString[IndProc+1] == 'J' )
        {
            strProcTemp = ProtocolAnaliserString[IndProc+1];
        }

    }
/*    while (strProcTemp == "L")
    {
        IndProc++;
        Name += ProtocolAnaliserString[IndProc];

        if (ProtocolAnaliserString[IndProc+1] == '"'|| ProtocolAnaliserString[IndProc+1] == '\0')
        {
            (*player)[_ID]->PlayerName = Name;
            printf("nome:%s\n",Name.c_str());
            strProcTemp = ProtocolAnaliserString[IndProc+1];
            if (ProtocolAnaliserString[IndProc+1] != '\0')
            {
                IndProc++;
                if (ProtocolAnaliserString[IndProc+1] == 'C')
                    strProcTemp = ProtocolAnaliserString[IndProc+1];
            }
        }
    }
    std::string chat = "";
    IndProc++;
    while (strProcTemp == "C")
    {
        IndProc++;
        chat += ProtocolAnaliserString[IndProc];
        if (ProtocolAnaliserString[IndProc+1] == '\0')
        {
            strProcTemp = '\0';
            (*player)[_ID]->stringChat =  chat;
            printf("chat: %s\n",chat.c_str());
            break;
        }
    }
*/
    //printf("Angulo: %d do cliente %d\n", (*player)[_ID]->AngleY,_ID);

}

