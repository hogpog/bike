
/*
 *
 *
 *
 *
 *
 *
 */

#ifndef _Assert_h
#define _Assert_h

#ifdef Assert
# undef Assert
#endif

#ifndef __STRING
# define Assert(a) ONE::Die(a, __FILE__, __LINE__, __FUNCTION__, #a)
#else
# define Assert(a) ONE::Die(a, __FILE__, __LINE__, __FUNCTION__, __STRING(a))
#endif



namespace ONE
{
	void Die(bool a, const char* file, int line, const char* func, const char* assert_str);
}

#endif
