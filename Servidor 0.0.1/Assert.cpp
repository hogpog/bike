
/*
 *
 *
 *
 *
 *
 *
 */

#include "Assert.h"

#include <stdio.h>
#include <stdlib.h>



namespace ONE
{
	void Die(bool a, const char* file, int line, const char* func, const char* assert_str)
	{
		if (a)
		{
			return;
		}

		#ifdef DEBUG
			fprintf(stderr, "Assertion Failed: '%s' file %s line %d in function %s\n", assert_str, file, line, func);
		#else
			fprintf(stderr, "Assertion Failed: '%s' file %s line %d in function %s\n", assert_str, file, line, func);
		#endif

		abort();
	}
}
