
/*
 *
 *
 *
 *
 *
 *
 */

#include "ThreadGroup.h"
#include "Thread.h"



namespace ONE
{
	ThreadGroup::ThreadGroup(const string& name)
	: m_GroupName(name)
	{
	}

	ThreadGroup::~ThreadGroup()
	{
		m_Threads.clear();
	}

	void ThreadGroup::addThread(Thread* thread)
	{
		m_ThreadIter = std::find(m_Threads.begin(), m_Threads.end(), thread);
		if (m_ThreadIter == m_Threads.end()) {
			m_Threads.push_back(thread);
		}
		else {
			// Duplicate Item
		}
	}

	void ThreadGroup::removeThread(Thread* thread)
	{
		if (m_Threads.size() > 0)
		{
			m_ThreadIter = std::find(m_Threads.begin(), m_Threads.end(), thread);
			if (m_ThreadIter != m_Threads.end()) {
				m_Threads.erase(m_ThreadIter);
			}
			else {
				// Doesn't Exist
			}
		}
	}

	void ThreadGroup::setName(const string& name)
	{
		m_GroupName = name;
	}

	const string& ThreadGroup::getName() const
	{
		return m_GroupName;
	}

	void ThreadGroup::Lock()
	{
		Thread* pThread = 0;
		m_ThreadIter = m_Threads.begin();
		while (m_ThreadIter != m_Threads.end())
		{
			if ((pThread = *m_ThreadIter) != 0)
				 pThread->Suspend();

			m_ThreadIter++;
		}
	}

	void ThreadGroup::Unlock()
	{
		Thread* pThread = 0;
		m_ThreadIter = m_Threads.begin();
		while (m_ThreadIter != m_Threads.end())
		{
			if ((pThread = *m_ThreadIter) != 0)
				 pThread->Resume();

			m_ThreadIter++;
		}
	}

	int ThreadGroup::getActivesThread()
	{
		return m_Threads.size();
	}
}
