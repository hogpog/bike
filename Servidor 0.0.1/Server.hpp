#ifndef _SERVER_HPP_
#define _SERVER_HPP_

#include <SDL_net.h>
#include <vector>
#include "Receiver.hpp"
#include "Send.hpp"
#include "Mutex.h"
#include "player.hpp"
#include "Process.hpp"
#include "Physics.hpp"
#include "types.hpp"

#define PORT 7121

class CServer:public ONE::Thread
{
	public:
	////Construtor padr�o
		CServer();
    //Destrutor padr�o
		~CServer();
    //Metodo virtual da classe thread(� oq roda na thread)
		void Run();
    //Pausa as threads do servidor
		void Pause();
    //Para todas as threads e o servidor
		void Stop();
    //Pega o ponteiro e mutex para o log
        void getLog(std::deque<std::string> *_log, Mutex* _logMutex);
	private:
	//verifica se esta parado
	bool isPaused;
	//porta do servidor
	Uint16 port;
	//mutex das threads
    Mutex* mutex[2];

    //vetor das threads de processamento
	std::vector<Process*>process;
	//vetor das threads de recebimento
	std::vector<CReceiver*>recv;
	//vetor das threads de envio
	std::vector<Send*>send;
	//Socket do servidor
	TCPsocket ServerSocket;
	//Ip do servidor
	IPaddress ServerIP;
	//Sockets dos clientes
	TCPsocket ClientSocket;
	//Ip do cliente
	IPaddress* ClientIP;
	//Dados dos players
	std::vector<Player*> player;
	//Log
	std::deque<std::string> *log;
	//Mutex para o log
	Mutex *logMutex;
	//Fisica
	CPhysics *Physic;


};


#endif
