
/*
 *
 *
 *
 *
 *
 *
 */

#ifndef _ThreadGroup_h
#define _ThreadGroup_h

#include "Globals.h"

#include <string>
#include <vector>
#include <algorithm>



namespace ONE
{
	class Thread;
	using std::string;
	using std::vector;
	typedef vector<Thread*> ThreadsVect;
	typedef ThreadsVect::iterator ThreadIter;

	class DLL_EXPORT ThreadGroup
	{
		public:
			ThreadGroup(const string& name);
			~ThreadGroup();

			void addThread(Thread* thread);
			void removeThread(Thread* thread);
			void setName(const string& name);
			const string& getName() const;
			void Lock();
			void Unlock();
			int getActivesThread();

		private:
			string m_GroupName;
			ThreadsVect m_Threads;
			ThreadIter m_ThreadIter;
	};
}

#endif
