#ifndef _PROCESS_HPP_
#define _PROCESS_HPP_

#include "Thread.h"
#include <deque>
#include <vector>
#include <string>
#include "player.hpp"
#include "SDL.h"
#include "types.hpp"



class Process:public ONE::Thread
{
	public:
		Process(std::vector<Player*> *_player, Mutex *_mutex[2],int _ID);
		void newProcess(std::vector<Player*> *_player, Mutex *_mutex[2], int _ID);
		~Process();
		void Run();
	private:
        std::string processedString;
        void ProtocolAnaliser(int _ID);
        Mutex *mutex[2];
        std::vector<Player*> *player;
        //int ID;
};

#endif
