#include "Server.hpp"

CServer::CServer()
{

    isPaused = false;
    port = PORT;
    if (!SDL_Init(0))
    {
        if (SDLNet_Init()<0)
        {
            printf("SDLNet_Init: %s\n",SDLNet_GetError());
            exit(0);
        }
    }
//	ClientSocket.push_back(0);
//	ClientIP.push_back(0);
    printf("Iniciando\n");
    if (SDLNet_ResolveHost(&ServerIP,NULL,port)<0)
    {
        printf("SDLNet_ResolveHost: %s\n",SDLNet_GetError());
        exit(1);
    }
    if (!(ServerSocket = SDLNet_TCP_Open(&ServerIP)))
    {
        fprintf(stderr, "SDLNet_TCP_Open: %s\n", SDLNet_GetError());
        exit(1);
    }

    printf("Iniciado\n");
    mutex[0] = new ONE::Mutex();
    mutex[1] = new ONE::Mutex();
    Physic = new CPhysics(&player);
    Physic->Start();
}

CServer::~CServer()
{
}
void CServer::Run()
{
    printf("Aguardando Conexao\n");
    logMutex->Lock();
    log->push_back("Servidor Iniciado\n");
    logMutex->Unlock();

    while (1)
    {
        //Physic->Draw();
        //printf("Aguardando Conexao1\n");
        if (ClientSocket=SDLNet_TCP_Accept(ServerSocket))
        {
            logMutex->Lock();
            log->push_back("Cliente Conectado\n");
            logMutex->Unlock();

            ClientIP = (SDLNet_TCP_GetPeerAddress(ClientSocket));
            //printf("Aguardando Conexao3\n");
            if (ClientIP)
            {
                /// Verifica o Slot Vago
                int FreeID;
                bool isFree = false;
                for (int FreeSocket = 0; FreeSocket<player.size();FreeSocket++)
                {
                    if (player[FreeSocket]->connected==false)
                    {
                        FreeID = FreeSocket+1;
                        isFree = true;
                        Physic->toAddPlayer(FreeSocket);
                        break;
                    }
                }
                ///Se tiver algum Slot Vago
                if (isFree)
                {
                    player[FreeID-1]->id = FreeID;
                    Physic->toAddPlayer(FreeID);
                    recv[FreeID-1]->newCReceiver(ClientSocket,player[FreeID-1],mutex[0]);
                    recv[FreeID-1]->Start();
                    send[FreeID-1]->newSend(ClientSocket,player[FreeID-1],mutex[1]);
                    send[FreeID-1]->Start();
                    process[FreeID-1]->newProcess(&player,mutex,FreeID-1);
                    process[FreeID-1]->Start();
                    player[FreeID-1]->connected = true;
                    isFree = false;
                }
                ///Se n�o tiver algum Slot Vago
                else
                {

                    player.push_back((new Player));
                    player[player.size()-1]->id=player.size();

                    Physic->addPlayer();

                    recv.push_back((new CReceiver(
                                        ClientSocket,
                                            player[player.size()-1],
                                                mutex[0])));
                    recv[recv.size()-1]->Start();
                    recv[recv.size()-1]->getLog(log,logMutex);

                    send.push_back((new Send(
                                        ClientSocket,
                                            player[player.size()-1],
                                                mutex[1])));
                    send[send.size()-1]->Start();

                    process.push_back((new Process(&player,mutex,send.size()-1)));
                    process[process.size()-1]->Start();

                    player[player.size()-1]->connected = true;
                }

            }
            else
            {
                printf("SDLNet_TCP_GetPeerAddress: %s\n",SDLNet_GetError());
            }
        }
        SDL_Delay(1);
    }
}

void CServer::Pause()
{/*
    if (!isPaused)
    {
        isPaused = true;
        printf("Suspending...\n");
        for (int x = 0; x< send.size();x++)
            send[x]->Suspend();
        for (int x = 0; x< recv.size();x++)
            recv[x]->Suspend();
        for (int x = 0; x< process.size();x++)
            process[x]->Suspend();
        this->Suspend();
        logMutex->Lock();
        log->push_back("Servidor em pausa.\n");
        logMutex->Unlock();
    }
    else
    {
        isPaused = false;
        printf("Resuming...\n");
        for (int x = 0; x< send.size();x++)
            send[x]->Resume();
        for (int x = 0; x< recv.size();x++)
            recv[x]->Resume();
        for (int x = 0; x< process.size();x++)
            process[x]->Resume();
        this->Resume();
        logMutex->Lock();
        log->push_back("Servidor rodando.\n");
        logMutex->Unlock();
    }
*/
}

void CServer::Stop()
{

///Ainda nao implmentado
    /*
            printf("Stopping...\n");
        for(int x = 0; x< send.size();x++)
            send[x]->Suspend();
        for(int x = 0; x< recv.size();x++)
            recv[x]->Suspend();
        for(int x = 0; x< process.size();x++)
            process[x]->Suspend();
        this->Suspend();
        for(int x = 0; x< send.size();x++)
            delete send[x];
        for(int x = 0; x< recv.size();x++)
            delete recv[x];
        for(int x = 0; x< process.size();x++)
            delete process[x];
            */
}

void CServer::getLog(std::deque<std::string> *_log, Mutex* _logMutex)
{
    log = _log;
    logMutex = _logMutex;
}
