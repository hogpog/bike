#ifndef _Physics_HPP_
#define _Physics_HPP_

#include <vector>
#include "Thread.h"
#include "types.hpp"
#include "player.hpp"


class CPhysics: public ONE::Thread
{
public:
    CPhysics(std::vector<Player*> *_player);
    ~CPhysics();
    void Run();
    void addPlayer();
    std::vector<core::vector3df> *position;
    void Draw();
    unsigned int times;
    void toAddPlayer(int id);
protected:

    void playerDataToPhysic();
    void getPhysicResults();
    bool isAddPlayer;
    std::vector<Player*> *player;

    IVideoDriver* driver;

    IWorld* p_world;
    ITreeBody* world_p_node;
    std::vector<ICharacterController*> rigell_body;

    IMaterial* rigell_material;
    IMaterial* level_material;

    std::vector<ISceneNode*> node;
    ICameraSceneNode* cam;

    IrrlichtDevice* device;
    ISceneManager* smgr;

    ISceneNode* levelNode;
    IAnimatedMesh* levelMesh;

    irr::core::line3d<irr::f32> lineDown;
    irr::core::line3d<irr::f32> lineFront;
    irr::core::line3d<irr::f32> lineBack;
    irr::newton::SIntersectionPoint out;

    scene::ITriangleSelector* selector;


private:

};


#endif
